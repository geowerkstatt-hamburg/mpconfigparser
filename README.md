> Switch to the [German version](./README.de.md).

# MP Config Parser

This is a package offering the tools and definitions needed to parse a [Masterportal](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/) `config.json.md` file. It returns a graph structure describing the options for a `config.json` file, and a log of warnings indicating structural errors within the `config.json.md` itself.

The [Masterportal Admintool](https://bitbucket.org/geowerkstatt-hamburg/mp-admintool/src/master/) uses the returned graph to generate forms. The Masterportal uses the warning log to validate its `config.json.md` file and to check whether German and English documentation are structurally identical.

```bash
# CLI tool usage
node ./node_modules/mpconfigparser/cli.js ./doc/config.json.md ./doc/config.json.de.md
```

In the following, the `config.json.md`'s structure is described.

When ‘config’ is used, it referes to the entire ‘config.json’.

The notation ε, ε2, ... denotes a dot-separated list of title fragments, e.g. "Portalconfig.menu.tools".

## Document structure

* The h1 chapter name starting with "config.json" defines the configuration's root node
    * The h2 chapter "layerConfig" is used for a separate (non-generated) form
    * The h2 chapter "Datatypes" is interpreted for referencing, but resides outside the chapter order; thus, no key "Datatypes" can be present in the config.json's root level
      * "Datatypes.Extent" and "Datatypes.Coordinate" are special types; their meaning cannot be overridden by changing their markdown entries
    * All other h2 chapters are used for form generation
    * An h2 chapter "version" is forbidden: This field is used to store version information
* Paragraphs/lists/... are translated to HTML as given and are used as help texts
* Contents not included within the h1 chapter "config.json" are ignored
    * Table of Content, further links, ...

## Admintool structure based on chapters

* h1: Not displayed
* h2: Each h2 is a configuration step (step in sidebar)
* h3: Each h3 is a sub-step to a step (visible in sidebar as step sub-chapter)
* h4+: A sub-chapter to h2 resp. h3, visually highlighted as a box
    * If a chapter is to have multiple form regions, but no sub-chapters in the sidebar, h2 must directly contain h4

## Title structure

* Case 1: Titles describe object nesting
    * For a chapter titled ε, the object config.ε is described
    * For a chapter ε with ε = ε2.x, where ε2 is not empty, at least one row in the tabular description of ε2 has type x
* Case 2: Titles describe a type
    * For a chapter titled ε, an object config.ε does not exist
    * For a chapter titled ε, an ε2 exists with
        * ε2 matches the rules of case 1 or case 2
        * ε2 references as type or inherits from ε

This structure describes a circle-free graph, where nodes are chapters. Edges are

* Child references (object nesting)
* Cross-references (object nesting)
* Inheritance relationships

Titles may also feature a suffixed ` {data-toc-label='Arbitrary label'}` at their end. These titles are kept in the graph as labels, but yield no structural information.

## Reference structure

The pattern `[a]: # (ε)` is used, where a describes the type of relation, and ε the referenced path. This structure is not visible in Markdown viewers and intentionally misappropriated here.

### Child reference

Direct references are modeled by ε using type x with ε.x existing.

### Cross-reference

After a title and empty line, the line `[type:x]: # (ε)` indicates a cross-reference. This defines that, *within this chapter*, the object "x" is to be structured as described in ε. The same chapter must not contain two definitions `[type:x]` and `[type:y]` with `x.toLowerCase() === y.toLowerCase()`.

### Inheritance

After a title and empty line, the line `[inherits]: # (ε)` indicates inheritance. All lines of ε are used as basis, and the new definition is copied over it.

Furthermore, the inheriting chapter may be used instead of the chapter inherited from. E.g., when A inherits from B, and C type B for an entry, then B may be used in C instead of A. This also holds transitively.

Abstract chapters can implicitly be declared by never being used as type. E.g., when A inherits from B, but B is never used as a type, B is effectively abstract. This technique can be used to pull common property sets.

## Table structure

Tables describe object structures. Starting from chapter depth h2, all tables are interpreted. Tables have the columns as described in the following order. The column "Expert" is currently optional - when not present, it's assumed that no table rows belong to the expert mode; that is, `false` is assumed as its value.

|Name|Required|Type|Default|Description|Expert|
|----|-------------|---|-------|------------|--------|
|Arbitrary string allowed as object key. The name is used as label after transformation from camelCase to Title Case.|"yes" or "no" are allowed. If "yes" the user must supply a truthy value.|see below|Default value for this entry. Must be readable with `JSON.parse` and match the type.|Arbitrary description shown on the generated form's input element. Should the text contain "@deprecated", this is shown as a hint, and the field is set read-only.|true or false; if true, the element is only shown in expert mode.|

### Type

The field type expects a text matching the following pattern. The user will be offered input elements matching the type.

#### Primitive

* `Number`
* `Boolean`
* `String`

#### Self-defined

* `Integer` (whole numbers)
* `Float` (synonym to `Number`)
* `Coordinate` (chosen by map)
* `Extent` (chosen by map)
* `LayerId` (chosen by dropdown)
* `RestId` (chosen by dropdown)
* `StyleId` (chosen by dropdown)

#### Enums

* `enum[1,2,3]`
    * This would allow 1, 2, and 3 as enum options
    * All primitive types are allowed

#### Object references

For any string x not matching by the preceding rules it is assumed that a named object is meant.

* Should a cross-reference as defined in "Reference structure" be defined, it is resolved here
* Else, it is assumed x is within ε so that ε.x exists

#### Arrays

Any preceding type may be suffixed with [] to allow setting an arbitrary amount of the specified type.

#### Or

If multiple types are allowed for a field, they may be separated by a "/". In that case, the user is presented radio buttons to choose the desired type.
