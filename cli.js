/* globals process console */
/* eslint-disable one-var, no-sync */

/**
 * Formats an error and throws it to stop the process.
 * @param {Graph} graph graph built from file at filename
 * @param {string} filename name of the file that produced the warnings
 * @throws {Error}
 * @returns {void}
 */
function errorDueToGraphWarnings (graph, filename) {
    throw new Error(`⚠️ Interpreting ${filename} yielded ${graph.warnings.length} warning(s). The current state of the ${filename} may result in unexpected behaviour when used in the mp-admintool, and is not well-formed.`);
}

/**
 * @param {Array} a1 compare array 1
 * @param {Array} a2 compare array 2
 * @returns {boolean} true if both arrays are deeply identical (no object deepness included)
 */
function arraysEqual (a1, a2) {
    return a1.length === a2.length &&
        a1.every((value, index) => Array.isArray(value)
            ? arraysEqual(value, a2[index])
            : value === a2[index]);
}

/**
 * Ensures printability of FieldType definition; else, e.g. the constructor 'String'
 * would simply become 'null' in JSON.stringify, which is lacking information.
 * @param {(FieldType|FieldType[])} types FieldType for array thereof for an edge property
 * @returns {(Array|*)} printable with JSON.stringify
 */
function makeTypeArrayPrintable (types) {
    if (Array.isArray(types)) {
        return types.map(type => makeTypeArrayPrintable(type));
    }
    return typeof types === "function" ? "Constructor " + types.name : types;
}

/*
 * This CLI script
 *  1. parses one or two config.json.md files from paths given as cli parameters to graph structures
 *  2. errors if a graph holds warnings (parsing already printed those)
 *  3. compares graphs structurally when two are given, printing differences; if differences are found, errors
 *
 * Example: `node ./<path>/cli.js ./docs/config.json.md ./docs/config.json.de.md`
 */

const parseMarkdown = require("./dist/index.js").parseMarkdown;
const fs = require("fs");

let filename1, filename2;

// check for correct tool usage - at least one filename is required
if (process.argv.length < 3) {
    console.error("Usage example: 'node " + process.argv[1] + " ./docs/config.json.md'. You may add a second file system link to also check if both files have the same structure.");

    throw new Error("Stopped @masterportal/mpconfigparser script due to missing cli parameters.");
}
else {
    filename1 = process.argv[2];
    filename2 = process.argv[3];
}

// parse first config, check for warnings
const config1 = fs.readFileSync(filename1, "utf8");
const graph1 = parseMarkdown(config1, filename1);

if (graph1.warnings.length) {
    errorDueToGraphWarnings(graph1, filename1);
}

// If only one file is given, the script ends here; else, the second graph is checked, and the resulting graphs are compared

if (filename2) {
    // check second config
    const config2 = fs.readFileSync(filename2, "utf8");
    const graph2 = parseMarkdown(config2, filename2);
    if (graph2.warnings.length) {
        errorDueToGraphWarnings(graph2, filename2);
    }

    // check sameness of graph1 and graph2

    // errors are logged asap to avoid suppressing messages on unexpected consequential failure
    const errors = [];
    errors.pushLog = (entry, first, second) => {
        const constructed = `❌ ${entry}` + (typeof first !== "undefined" ? `:
    ${filename1}: ${JSON.stringify(first)}
    ${filename2}: ${JSON.stringify(second)}` : "");
        console.error(constructed);
        errors.push(constructed);
    };

    if (!arraysEqual(graph1.chapterOrder, graph2.chapterOrder)) {
        errors.pushLog("Global chapter order differs between graphs", graph1.chapterOrder, graph2.chapterOrder);
    }

    const graph1NodeKeys = Object.keys(graph1.nodes);
    const graph2NodeKeys = Object.keys(graph2.nodes);

    if (!arraysEqual(graph1NodeKeys, graph2NodeKeys)) {
        errors.pushLog("Graphs offered different nodes", graph1NodeKeys, graph2NodeKeys);
    }

    graph1NodeKeys.forEach(nodeName => {
        const node1 = graph1.nodes[nodeName];
        const node2 = graph2.nodes[nodeName];

        // skip if one of the nodes does not exist
        if (!node1 || !node2) {
            errors.pushLog(`Skipping node comparison for '${nodeName}' since node does not exist in '${node1 ? filename2 : filename1}'.`);
            return;
        }

        // compare node properties
        [
            "name",
            "label",
            "chapterKey",
            "h",
            "inherits",
            "path"
            // "outgoingOrder" is checked below in a separate step since it's more complex
        ].forEach(nodeProperty => {
            if (Array.isArray(node1[nodeProperty])
                ? !arraysEqual(node1[nodeProperty], node2[nodeProperty])
                : node1[nodeProperty] !== node2[nodeProperty]) {
                errors.pushLog(`Nodes were different for '${nodeName}' in property '${nodeProperty}'`,
                    node1[nodeProperty],
                    node2[nodeProperty]);
            }
        });

        // compare nodes' edges; descriptive contents (text, examples, property description, ...) are fully ignored
        node1.outgoingOrder
            .filter(edgeName => !edgeName.includes("__mpadminkey_html"))
            .forEach(edgeName => {
                const edge1 = node1.outgoing[edgeName];
                const edge2 = node2.outgoing[edgeName];

                // skip if one of the nodes does not exist
                if (!edge1 || !edge2) {
                    errors.pushLog(`Skipping edge '${edgeName}' comparison for '${nodeName}' since edge does not exist in '${edge1 ? filename2 : filename1}'.`);
                    return;
                }

                // compare edge properties
                [
                    "name",
                    "label",
                    "required",
                    "deprecated",
                    "expert",
                    "default"
                ].forEach(propertyName => {
                    // default is stringified before comparison for simple object/array deepEquals
                    if (propertyName === "default"
                        ? JSON.stringify(edge1[propertyName]) !== JSON.stringify(edge2[propertyName])
                        : edge1[propertyName] !== edge2[propertyName]) {
                        errors.pushLog(`Edges '${edgeName}' were different for '${nodeName}' in property '${propertyName}'`,
                            edge1[propertyName],
                            edge2[propertyName]);
                    }
                });

                // types have to be prepared before printing so that the difference becomes visible in JSON.stringify
                if (!arraysEqual(edge1.types, edge2.types)) {
                    errors.pushLog(`Edges '${edgeName}' were different for '${nodeName}' in property 'types'`,
                        edge1.types.map(makeTypeArrayPrintable),
                        edge2.types.map(makeTypeArrayPrintable));
                }
            });
    });

    if (errors.length) {
        throw new Error(`⚠️ ${errors.length} error(s) occurred during comparison of '${filename1}' and '${filename2}'. The files must structurally match for a well-formed config.json definition. The mp-admintool may show unexpected behaviour.`);
    }
}
