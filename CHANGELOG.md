# CHANGELOG

## 2.0.0

* Breaking: The MpConfigParser can no longer produce empty graphs, and will throw an error on detecting an empty graph was produced. This is intentional to prevent a practically erroneous situation that may occur on not detecting the "# config.json" chapter.
* Breaking: The MpConfigParser will now generate a warning on finding "empty chapters". Such chapters technically model objects that can not have any keys. Should you have modeled your code in this way, please opt for a `boolean` or `enum` instead.
* Breaking: The input definition in the README.md has been updated with the following changes:
  * "Themenconfig" is now named "layerConfig".
  * The h2 chapter "Datatypes" is now read, but not added to the chapter order. "Datatypes.Coordinate" and "Datatypes.Extent" remain special types that can not be influenced by markdown changes. All other sub-chapters may now specify types that are interpreted and usable.
* Feature: The root node of the document is no longer required to be named "config.json", but is allowed to be named anything that matches `/^config\.json/g`, e.g. "config.json 3.0" (and future versions) now fit. Exactly one matching h1 must be present.
* Feature: The MpConfigParser no longer filters out labels used for MkDocs, and keeps and forwards such labels now.
  * Chapter labels with suffix ` { data-toc-label="Label" }` are now kept as `dataTocLabel` on Graph.Node elements.
  * Code fence titles (like \`\`\`json title="Example"\`\`\`) are now added as title to the produced HTML fragments; to the previous example an HTML fragment `<code title="Example">...</code>` is produced now. Previously, the title was simply ignored.
* Fix: Remove implicit `markdown-header-` prefix requirement on cross-chapter linkage.

## 1.4.0

* Feature: The MpConfigParser now filters out labels used for MkDocs.

## 1.3.1

* Bugfix: Links to the old bitbucket repository (lgv-g12) have been replaced with the new URL (geowerkstatt).
* Bugfix: Corrected filenames in config.json.md check run error messages.
* Chore: Dependencies have been updated.

## 1.3.0
* Feature: A new type `styleId` was added.
* Feature: A new type `restId` was added.

## 1.2.0

* Chore: The documentation has been translated to English.
* Feature: A new type `layerId` was added.

## 1.1.2

* Bugfix: Recursive config.json.md structures would crash the parser in prior versions

## 1.1.1

* Bugfix: Parser did not recognize "no"/"yes" as input for the "required" column, only "nein"/"ja". Both English and German notations are recognized now.
* Bugfix: CLI script would error on perfectly fine config files with zero warnings. This issue was resolved.
* Bugfix: Config.json comparison threw on differently formatted paragraphs when comparing languages. It does no longer throw an error in such circumstances.
* Bugfix: Inheritors were not considered to be part of the graph on validity checks. They are now included when their parent is included.

## 1.1.0

Feature: The included `cli.js` script file has been extended to interpret a second config.json.md path. If two paths are given, both graphs are structurally compared to ensure equality. This is implemented to compare config.json.md files in different languages. Translatable contents are not compared.

## 1.0.0

Initial release.
