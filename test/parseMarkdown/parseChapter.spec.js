import markdownIt from "markdown-it";
import {createGraph} from "parseMarkdown/graph";
import parseChapter from "parseMarkdown/parseChapter";

const md = markdownIt(),
    snippets = {
        header: "## chapter.header",
        table: `## arbitrary

|a|b|c|d|e|f|
|-|-|-|-|-|-|
|name|ja|String|"asdf"|sadf|false|
`,
        text: `## arbitrary

mister paragraph

> doctor blockquote

\`\`\`json title="eXaMpLe"
    professor fence
\`\`\`
`,
        list: `## arbitrary

* a
* b
    * c
    * d
        1. e
    * f
* g

1. a
2. b
    * c
        * d
            1. e
3. f
`
};

describe("parseMarkdown/parseChapter", function () {
    it("reads header as expected", function () {
        const graph = createGraph(),
            links = {},
            tokens = md.parse(snippets.header, links);
        parseChapter(graph, tokens, links, md);

        expect(graph.nodes["chapter.header"].chapterKey).toBe("chapter.header");
        expect(graph.nodes["chapter.header"].path).toEqual(["chapter"]);
        expect(graph.nodes["chapter.header"].name).toEqual("header");
        expect(graph.nodes["chapter.header"].label).toEqual("Header");
        expect(graph.nodes["chapter.header"].h).toBe(2);
    });

    it("parses table rows in fixed format", function () {
        const graph = createGraph(),
            links = {},
            tokens = md.parse(snippets.table, links);
        parseChapter(graph, tokens, links, md);

        expect(graph.nodes.arbitrary.outgoing.name).toEqual({
            name: "name",
            label: "Name",
            required: true,
            deprecated: false,
            types: [String],
            default: "asdf",
            description: "sadf",
            expert: false
        });
    });

    it("parses paragraphs and blockquotes", function () {
        const graph = createGraph(),
            links = {},
            tokens = md.parse(snippets.text, links);

        parseChapter(graph, tokens, links, md);

        expect(graph.nodes.arbitrary.outgoing).toEqual({
            __mpadminkey_html0: "<p>mister paragraph</p>",
            __mpadminkey_html1: "<blockquote>doctor blockquote</blockquote>",
            __mpadminkey_html2: `<figure><figcaption>eXaMpLe</figcaption><code>    professor fence
</code></figure>`
        });
    });

    it("reads lists as expected", function () {
        const graph = createGraph(),
            links = {},
            tokens = md.parse(snippets.list, links);
        parseChapter(graph, tokens, links, md);

        expect(graph.nodes.arbitrary.outgoing.__mpadminkey_html0).toBe("<ul><li>a</li><li>b</li><ul><li>c</li><li>d</li><ol><li>e</li></ol><li>f</li></ul><li>g</li></ul>");
        expect(graph.nodes.arbitrary.outgoing.__mpadminkey_html1).toBe("<ol><li>a</li><li>b</li><ul><li>c</li><ul><li>d</li><ol><li>e</li></ol></ul></ul><li>f</li></ol>");
        expect(graph.nodes.arbitrary.outgoing.__mpadminkey_html2).toBeUndefined();
    });
});
