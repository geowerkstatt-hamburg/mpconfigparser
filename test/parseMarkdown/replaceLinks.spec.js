import replaceLinks, {documentationTarget, fileTarget, externalTarget, baseUrl} from "parseMarkdown/replaceLinks";

const links = [
        "<a href=\"www.example.com\">Link</a>",
        "<a href=\"services.json\">Link</a>",
        "<a href=\"services.json.md\">Link</a>",
        "<a href=\"#markdown.chapter\">Link</a>"
    ],
    expectedResults = [
        `<a target="${externalTarget}" href="www.example.com">Link</a>`,
        `<a target="${fileTarget}" href="${baseUrl}services.json">Link</a>`,
        `<a target="${fileTarget}" href="${baseUrl}services.json.md">Link</a>`,
        `<a target="${documentationTarget}" href="${baseUrl}config.json.md#markdown.chapter">Link</a>`
    ],
    filler = "text between links\n";

describe("parseMarkdown/replaceLinks", function () {
    it("replaces anchors with full anchored links to config", function () {
        expect(replaceLinks(links[3])).toEqual(expectedResults[3]);
    });

    it("replaces relative file references with absolute file references", function () {
        expect(replaceLinks(links[1])).toEqual(expectedResults[1]);
        expect(replaceLinks(links[2])).toEqual(expectedResults[2]);
    });

    it("targets external references to a separate tab", function () {
        expect(replaceLinks(links[0])).toEqual(expectedResults[0]);
    });

    it("works for arbitrary orders", function () {
        const permutations = [...links.keys()].reduce(function permute (res, item, key, arr) {
            return res.concat(arr.length > 1 && arr.slice(0, key).concat(arr.slice(key + 1)).reduce(permute, []).map(function (perm) {
                return [item].concat(perm);
            }) || item);
        }, []);

        permutations.forEach(p => {
            const replaced = replaceLinks(p.map(i => links[i]).join(filler)),
                expected = p.map(i => expectedResults[i]).join(filler);

            expect(replaced).toEqual(expected);
        });
    });
});
