import {createNode, getReferencedNodeKeys, addEdge} from "parseMarkdown/graph";

describe("parseMarkdown/graph/Node", function () {
    describe("createNode", function () {
        it("creates a node with initialized chapterKey-related information", function () {
            const node = createNode({
                chapterKey: "Some.string.camelCase { data-toc-label='displayLabel' }",
                h: 1
            });

            expect(node.name).toBe("camelCase");
            expect(node.label).toBe("Camel Case");
            expect(node.dataTocLabel).toBe("displayLabel");
            expect(node.path).toEqual(["Some", "string"]);
            expect(node.h).toBe(1);
            expect(node.inherits).toBeNull();
            expect(node.outgoing).toEqual({});
            expect(node.outgoingOrder).toEqual([]);
        });

        it("creates a node with initialized inheritation-related information", function () {
            const node = createNode({
                chapterKey: "Some.string.camelCase",
                h: 1,
                inherits: "arbitrary.key"
            });

            expect(node.inherits).toBe("arbitrary.key");
        });
    });

    describe("addEdge", function () {
        it("adds string nodes with made-up key", function () {
            const node = createNode({chapterKey: "root", h: 2});

            addEdge(node, "<i>html1</i>");
            addEdge(node, "<i>html2</i>");
            addEdge(node, "<i>html3</i>");

            expect(node.outgoingOrder).toHaveLength(3);
            expect(node.outgoing[node.outgoingOrder[0]]).toBe("<i>html1</i>");
            expect(node.outgoing[node.outgoingOrder[1]]).toBe("<i>html2</i>");
            expect(node.outgoing[node.outgoingOrder[2]]).toBe("<i>html3</i>");
            expect(node.outgoingOrder).toEqual([
                "__mpadminkey_html0",
                "__mpadminkey_html1",
                "__mpadminkey_html2"
            ]);
        });

        it("adds prepared edge by its name", function () {
            const node = createNode({chapterKey: "root", h: 2});

            addEdge(node, {name: "a"});
            addEdge(node, {name: "b"});
            addEdge(node, {name: "c"});

            expect(node.outgoingOrder).toHaveLength(3);
            expect(node.outgoing[node.outgoingOrder[0]]).toEqual({name: "a"});
            expect(node.outgoing[node.outgoingOrder[1]]).toEqual({name: "b"});
            expect(node.outgoing[node.outgoingOrder[2]]).toEqual({name: "c"});
            expect(node.outgoingOrder).toEqual([
                "a",
                "b",
                "c"
            ]);
        });
    });

    describe("getReferencedNodeKeys", function () {
        it("returns every outgoing object reference once", function () {
            const mockNode = {
                outgoing: {
                    "A": "A",
                    "B": {
                        types: [
                            [Object, "type1"],
                            [Object, "type2"],
                            [Array, [Object, "type3"]]
                        ]
                    },
                    "C": "C",
                    "D": {
                        types: [
                            [Object, "type1"],
                            [Object, "type4"]
                        ]
                    }
                }
            },
                res = getReferencedNodeKeys(mockNode);

            expect(res).toEqual(expect.arrayContaining([
                "type1",
                "type2",
                "type3",
                "type4"
            ]));
            expect(res).toHaveLength(4);
        });

        it("does not return self-references", function () {
            const mockNode = {
                outgoing: {
                    x: {
                        types: [[Object, "B"]]
                    }
                },
                chapterKey: "B"
            },
                res = getReferencedNodeKeys(mockNode);

            expect(res).toEqual([]);
        });
    });
});
