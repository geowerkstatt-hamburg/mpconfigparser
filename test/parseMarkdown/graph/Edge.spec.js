import {createEdge} from "parseMarkdown/graph";

describe("parseMarkdown/graph/Edge", function () {
    describe("createEdge", function () {
        it("creates a new edge", function () {
            const e = createEdge();

            expect(e).toEqual({});
        });
    });
});
