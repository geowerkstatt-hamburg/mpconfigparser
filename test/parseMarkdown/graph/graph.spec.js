import {createGraph, createNode, addNode} from "parseMarkdown/graph";

describe("parseMarkdown/graph", function () {
    describe("createGraph", function () {
        it("creates an empty graph element", function () {
            const g = createGraph();
            expect(g.chapterOrder).toEqual([]);
            expect(g.nodes).toEqual({});
            expect(g.warnings).toEqual([]);
        });
    });

    describe("addNode", function () {
        it("registers h2 entries to the graph entry points", function () {
            const g = createGraph();

            addNode(g, createNode({chapterKey: "Some.string.camelCase0", h: 1}));
            addNode(g, createNode({chapterKey: "Some.string.camelCase1", h: 2}));
            addNode(g, createNode({chapterKey: "Some.other.camelCase2", h: 2}));
            addNode(g, createNode({chapterKey: "Some.third.camelCase3", h: 3}));

            expect(g.chapterOrder).toEqual([
                "Some.string.camelCase1",
                "Some.other.camelCase2"
            ]);

            expect(g.nodes[g.chapterOrder[0]].name).toEqual("camelCase1");
            expect(g.nodes[g.chapterOrder[1]].name).toEqual("camelCase2");
        });
    });
});
