TEST FILE - should contain every feature markdown parser offers

>Back to [documentation](www.example.com).

# config.json

Lorem *config.json* äüö#+!"§$%&/()=?\` Ipsum [Dolor](www.example.com/Sit) Amet.

## Portalconfig

Lorem *config.json* äüö#+!"§$%&/()=?\` Ipsum [Dolor](www.example.com/Sit) Amet.

Defines type "cross" as reference to crossreference.

[type:cross]: # (crossreference)

Beschreibungstext mit *vielartig* verschachtelten Listen.

1. a
2. b
    1. alpha
        * Α, α
    2. beta (Β, β)
    3. gamma
        * Γ
        * γ
3. c

* 1
* 2
* 3
    1. eins
    2. zwei

|Name|Verpflichtend|Typ|Default|Beschreibung|Expertenmodus|
|---|---|---|---|---|---|
|textEintrag|ja|String|"alphabet"|Beschreibungstext|false|
|textListe|ja|String[]||Beschreibungstext|false|
|boolscher|nein|Boolean|true|asdf|false|
|nummer|ja|Number|4.44|asdf|false|
|nummerListe|ja|Number[]|[]|asdf|false|
|schweben|nein|Float|4.44|asdf|false|
|vollzahl|ja|Integer||*asdf*|false|
|koordinate|nein|Coordinate|[5, 5]|asdf|false|
|ausdehnung|nein|Extent|[5, 5, 4, 4]|asdf|false|
|abgezaehlt|ja|enum["A", "B", "C"]|"A"|asdf|true|
|multiAbgezaehlt|ja|enum["A", "B", "C"][]|["A", "B"]|asdf|false|
|multiTypeEntry|ja|String/Boolean/Number/Number[]|5|asdf|false|
|nested|nein|**[nested](#markdown-header-portalconfignested)**|{"flo":3}|Beschreibungstext|false|
|nesteds|nein|**[nested](#markdown-header-portalconfignested)**[]||Beschreibungstext|false|
|k|nein|[cross](#markdown-header-crossreference)||asdf|false|

**Beispiel objektarray:**

```json title="eXaMpLe"
#!json

"objektarray": [
    {"a": 5},
    {"a": 6},
]
```

******

### Portalconfig.nested { data-toc-label='Portal Config Nested' }

Imports all table rows of crossreference.

[inherits]: # (crossreference)

|Name|Verpflichtend|Typ|Default|Beschreibung|
|---|---|---|---|---|
|textEintrag|ja|String|"alphabet"|Beschreibungstext|

## crossreference { data-toc-label='Cross Reference' }

Imports all table rows of Datatypes.inheritance.

[inherits]: # (Datatypes.inheritance)

|Name|Verpflichtend|Typ|Default|Beschreibung|
|---|---|---|---|---|
|int|nein|Integer||Beschreibungstext|

## Datatypes

### Datatypes.inheritance

|Name|Verpflichtend|Typ|Default|Beschreibung|
|---|---|---|---|---|
|flo|nein|Float|5.5|Beschreibungstext|

_________________________________________________

# another h1 ends the parsing process

This is not included in the graph.

## neither.is.this

|Name|Verpflichtend|Typ|Default|Beschreibung|
|---|---|---|---|---|
|feld|nein|String|"nor is this"|or this|

## more.descriptions

this, too, doesn't appear in the graph

|tables|are|
|---|---|
|not|interpreted|
