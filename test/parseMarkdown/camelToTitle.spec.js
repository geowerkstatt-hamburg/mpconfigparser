import camelToTitle from "parseMarkdown/camelToTitle";

describe("camelToTitle", function () {
    it("returns an empty string for an empty string", function () {
        expect(camelToTitle("")).toEqual("");
    });

    it("parses camel case strings to title case strings", function () {
        expect(camelToTitle("x")).toEqual("X");
        expect(camelToTitle("X")).toEqual("X");
        expect(camelToTitle("phi")).toEqual("Phi");
        expect(camelToTitle("Phi")).toEqual("Phi");
        expect(camelToTitle("testStringFactoryManager")).toEqual("Test String Factory Manager");
        expect(camelToTitle("TestStringFactoryManager")).toEqual("Test String Factory Manager");
    });

    it("handles CAPS (usually abbreviations) correctly", function () {
        expect(camelToTitle("USBPort")).toEqual("USB Port"); // caps first
        expect(camelToTitle("bestUSBPort")).toEqual("Best USB Port"); // caps middle
        expect(camelToTitle("bornInTheUSA")).toEqual("Born In The USA"); // caps last
        expect(camelToTitle("captainCAPSLOCK")).toEqual("Captain CAPSLOCK"); // for fun
    });

    it("handles numbers as lower-case letters", function () {
        expect(camelToTitle("43")).toEqual("43");
        expect(camelToTitle("41Test43")).toEqual("41 Test43");
        expect(camelToTitle("hunter2")).toEqual("Hunter2");
        expect(camelToTitle("Eggs12Milk2Carrots1336L33tsp34k")).toEqual("Eggs12 Milk2 Carrots1336 L33tsp34k");
    });
});
