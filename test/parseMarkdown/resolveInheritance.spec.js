import resolveInheritance from "parseMarkdown/resolveInheritance";

describe("parseMarkdown/resolveInheritance", function () {
    it("injects inherited fields right before its own fields, except duplicate keys", function () {
        const b = {types: []},
            c = {types: []},
            graph = {
                warnings: [],
                chapterOrder: ["a"],
                nodes: {
                    a: {
                        outgoingOrder: [
                            "x1",
                            "x2",
                            "b",
                            "x3"
                        ],
                        outgoing: {
                            x1: "asdf",
                            x2: "asdf",
                            b,
                            x3: "asdf"
                        },
                        inherits: "b",
                        chapterKey: "a"
                    },
                    b: {
                        outgoingOrder: [
                            "x1",
                            "x2",
                            "b",
                            "c",
                            "x3"
                        ],
                        outgoing: {
                            x1: "asdf",
                            x2: "asdf",
                            b: {types: []},
                            c,
                            x3: "asdf"
                        },
                        chapterKey: "b"
                    }
                }
            };

        resolveInheritance(graph);

        expect(graph.nodes.a.outgoingOrder).toEqual([
            "x1",
            "x2",
            "c",
            "b",
            "x3"
        ]);
        expect(graph.nodes.a.outgoing.b).toBe(b);
        expect(graph.nodes.a.outgoing.c).toBe(c);
    });

    it("field inheritance works for chained inheritance", function () {
        const b = {types: []},
            c = {types: []},
            d = {types: []},
            graph = {
                warnings: [],
                chapterOrder: ["a"],
                nodes: {
                    a: {
                        outgoingOrder: ["b"],
                        outgoing: {b},
                        inherits: "b",
                        chapterKey: "a"
                    },
                    b: {
                        outgoingOrder: ["c"],
                        outgoing: {c},
                        inherits: "c",
                        chapterKey: "b"
                    },
                    c: {
                        outgoingOrder: ["d"],
                        outgoing: {d},
                        chapterKey: "c"
                    }
                }
            };

        resolveInheritance(graph);

        expect(graph.nodes.a.outgoingOrder).toEqual([
            "d",
            "c",
            "b"
        ]);
        expect(graph.nodes.a.outgoing.b).toBe(b);
        expect(graph.nodes.a.outgoing.c).toBe(c);
        expect(graph.nodes.a.outgoing.d).toBe(d);

        expect(graph.nodes.b.outgoingOrder).toEqual([
            "d",
            "c"
        ]);
        expect(graph.nodes.b.outgoing.c).toBe(c);
        expect(graph.nodes.b.outgoing.d).toBe(d);

        expect(graph.nodes.c.outgoingOrder).toEqual(["d"]);
        expect(graph.nodes.c.outgoing.d).toBe(d);
    });

    /*
    TODO it is currently not clear if this should be a feature at all; see annotation in resolveInheritance.js where resolveInheritance is commented out
    it("inheriting chapters can be used instead of chapters inherited from; if A inherits from B, A is a B; this is done by type injection", function () {
        // // type structure:
        //         z
        //         |
        //         y
        //        / \
        //       x   x2
        //
        const graph = {
            warnings: [],
            chapterOrder: [
                "holderX2",
                "holderL",
                "holderM",
                "holderN",
                "holderLA",
                "holderMA",
                "holderNA"
            ],
            nodes: {
                holderX2: {
                    outgoingOrder: ["outX2"],
                    outgoing: {outX2: {types: [[Object, "x2"]]}}
                },
                holderL: {
                    outgoingOrder: ["outX"],
                    outgoing: {outX: {types: [[Object, "x"]]}}
                },
                holderM: {
                    outgoingOrder: ["outY"],
                    outgoing: {outY: {types: [[Object, "y"]]}}
                },
                holderN: {
                    outgoingOrder: ["outZ"],
                    outgoing: {outZ: {types: [[Object, "z"]]}}
                },
                holderLA: {
                    outgoingOrder: ["outX"],
                    outgoing: {outX: {types: [[Array, [Object, "x"]]]}}
                },
                holderMA: {
                    outgoingOrder: ["outY"],
                    outgoing: {outY: {types: [[Array, [Object, "y"]]]}}
                },
                holderNA: {
                    outgoingOrder: ["outZ"],
                    outgoing: {outZ: {types: [[Array, [Object, "z"]]]}}
                },
                x: {
                    outgoingOrder: ["b"],
                    outgoing: {b: {types: [String]}},
                    inherits: "y",
                    chapterKey: "x"
                },
                x2: {
                    outgoingOrder: ["b"],
                    outgoing: {b: {types: [String]}},
                    inherits: "y",
                    chapterKey: "x2"
                },
                y: {
                    outgoingOrder: ["c"],
                    outgoing: {c: {types: [String]}},
                    inherits: "z",
                    chapterKey: "y"
                },
                z: {
                    outgoingOrder: ["d"],
                    outgoing: {d: {types: [String]}},
                    chapterKey: "z"
                }
            }
        };

        resolveInheritance(graph);

        // leaf nodes are not affected
        expect(graph.nodes.holderL.outgoing.outX.types).toEqual(expect.arrayContaining([[Object, "x"]]));
        expect(graph.nodes.holderL.outgoing.outX.types).toHaveLength(1);
        expect(graph.nodes.holderLA.outgoing.outX.types).toEqual(expect.arrayContaining([[Array, [Object, "x"]]]));
        expect(graph.nodes.holderLA.outgoing.outX.types).toHaveLength(1);

        // y can be replaced with x and x2
        expect(graph.nodes.holderM.outgoing.outY.types).toEqual(expect.arrayContaining([
            [Object, "y"],
            [Object, "x"],
            [Object, "x2"]
        ]));
        expect(graph.nodes.holderM.outgoing.outY.types).toHaveLength(3);
        expect(graph.nodes.holderMA.outgoing.outY.types).toEqual(expect.arrayContaining([
            [Array, [Object, "y"]],
            [Array, [Object, "x"]],
            [Array, [Object, "x2"]]
        ]));
        expect(graph.nodes.holderMA.outgoing.outY.types).toHaveLength(3);

        // z can be replaced with y (which can be replaced with x or x2)
        expect(graph.nodes.holderN.outgoing.outZ.types).toEqual(expect.arrayContaining([
            [Object, "z"],
            [Object, "y"],
            [Object, "x"],
            [Object, "x2"]
        ]));
        expect(graph.nodes.holderN.outgoing.outZ.types).toHaveLength(4);
        expect(graph.nodes.holderNA.outgoing.outZ.types).toEqual(expect.arrayContaining([
            [Array, [Object, "z"]],
            [Array, [Object, "y"]],
            [Array, [Object, "x"]],
            [Array, [Object, "x2"]]
        ]));
        expect(graph.nodes.holderNA.outgoing.outZ.types).toHaveLength(4);
    });
    */
});
