import parseMarkdown from "parseMarkdown";
import {BasicFieldTypes} from "parseMarkdown/definitions";
import markdown from "./config.json.test.md";

const jsonCodeBlock = `<figure><figcaption>eXaMpLe</figcaption><code>#!json

"objektarray": [
    {"a": 5},
    {"a": 6},
]
</code></figure>`,
    warningMd = `
# config.json
## Portalconfig
|Name|Verpflichtend|Typ|Default|Expert|
|---|---|---|---|---|
|textEintrag|ja|broekn|"alphabet"|false|
`;

describe("parseMarkdown", function () {
    it("logs errors when found", () => {
        const warn = console.warn;
        console.warn = jest.fn();
        parseMarkdown(warningMd);
        expect(console.warn).toHaveBeenCalled();
        console.warn = warn;
    });

    it("reads the example file (parseMarkdown folder integration test)", function () {
        const graph = parseMarkdown(markdown);

        // quick-fail checks
        expect(graph.warnings).toHaveLength(0);
        expect(graph.nodes["neither.is.this"]).toBeUndefined();
        expect(graph.nodes.Portalconfig).toBeDefined();
        expect(graph.nodes.crossreference).toBeDefined();

        // big picture
        expect(graph).toEqual({
            warnings: [],
            chapterOrder: [
                "Portalconfig",
                "crossreference"
            ],
            nodes: {
                "Portalconfig": {
                    name: "Portalconfig",
                    label: "Portalconfig",
                    dataTocLabel: null,
                    path: [],
                    h: 2,
                    inherits: null,
                    outgoing: {
                        __mpadminkey_html0: "<p>Lorem <em>config.json</em> äüö#+!&quot;§$%&amp;/()=?` Ipsum <a target=\"_blank\" href=\"www.example.com/Sit\">Dolor</a> Amet.</p>",
                        __mpadminkey_html1: "<p>Defines type &quot;cross&quot; as reference to crossreference.</p>",
                        __mpadminkey_html2: "<p>Beschreibungstext mit <em>vielartig</em> verschachtelten Listen.</p>",
                        __mpadminkey_html3: "<ol><li>a</li><li>b</li><ol><li>alpha</li><ul><li>Α, α</li></ul><li>beta (Β, β)</li><li>gamma</li><ul><li>Γ</li><li>γ</li></ul></ol><li>c</li></ol>",
                        __mpadminkey_html4: "<ul><li>1</li><li>2</li><li>3</li><ol><li>eins</li><li>zwei</li></ol></ul>",
                        textEintrag: {
                            name: "textEintrag",
                            label: "Text Eintrag",
                            required: true,
                            deprecated: false,
                            types: [BasicFieldTypes.string],
                            default: "alphabet",
                            description: "Beschreibungstext",
                            expert: false
                        },
                        textListe: {
                            name: "textListe",
                            label: "Text Liste",
                            required: true,
                            deprecated: false,
                            types: [
                                [
                                    BasicFieldTypes.array,
                                    BasicFieldTypes.string
                                ]
                            ],
                            description: "Beschreibungstext",
                            expert: false
                        },
                        boolscher: {
                            name: "boolscher",
                            label: "Boolscher",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.boolean],
                            default: true,
                            description: "asdf",
                            expert: false
                        },
                        nummer: {
                            name: "nummer",
                            label: "Nummer",
                            required: true,
                            deprecated: false,
                            types: [BasicFieldTypes.number],
                            default: 4.44,
                            description: "asdf",
                            expert: false
                        },
                        nummerListe: {
                            name: "nummerListe",
                            label: "Nummer Liste",
                            required: true,
                            deprecated: false,
                            types: [
                                [
                                    BasicFieldTypes.array,
                                    BasicFieldTypes.number
                                ]
                            ],
                            default: [],
                            description: "asdf",
                            expert: false
                        },
                        schweben: {
                            name: "schweben",
                            label: "Schweben",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.float],
                            default: 4.44,
                            description: "asdf",
                            expert: false
                        },
                        vollzahl: {
                            name: "vollzahl",
                            label: "Vollzahl",
                            required: true,
                            deprecated: false,
                            types: [BasicFieldTypes.integer],
                            description: "<em>asdf</em>",
                            expert: false
                        },
                        koordinate: {
                            name: "koordinate",
                            label: "Koordinate",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.coordinate],
                            default: [5, 5],
                            description: "asdf",
                            expert: false
                        },
                        ausdehnung: {
                            name: "ausdehnung",
                            label: "Ausdehnung",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.extent],
                            default: [
                                5,
                                5,
                                4,
                                4
                            ],
                            description: "asdf",
                            expert: false
                        },
                        abgezaehlt: {
                            name: "abgezaehlt",
                            label: "Abgezaehlt",
                            required: true,
                            deprecated: false,
                            types: [
                                [
                                    BasicFieldTypes.enum,
                                    [
                                        "A",
                                        "B",
                                        "C"
                                    ]
                                ]
                            ],
                            default: "A",
                            description: "asdf",
                            expert: true
                        },
                        multiAbgezaehlt: {
                            name: "multiAbgezaehlt",
                            label: "Multi Abgezaehlt",
                            required: true,
                            deprecated: false,
                            types: [
                                [
                                    BasicFieldTypes.array,
                                    [
                                        BasicFieldTypes.enum,
                                        [
                                            "A",
                                            "B",
                                            "C"
                                        ]
                                    ]
                                ]
                            ],
                            default: ["A", "B"],
                            description: "asdf",
                            expert: false
                        },
                        multiTypeEntry: {
                            name: "multiTypeEntry",
                            label: "Multi Type Entry",
                            required: true,
                            deprecated: false,
                            types: [
                                BasicFieldTypes.string,
                                BasicFieldTypes.boolean,
                                BasicFieldTypes.number,
                                [
                                    BasicFieldTypes.array,
                                    BasicFieldTypes.number
                                ]
                            ],
                            default: 5,
                            description: "asdf",
                            expert: false
                        },
                        nested: {
                            name: "nested",
                            label: "Nested",
                            required: false,
                            deprecated: false,
                            types: [
                                [
                                    BasicFieldTypes.object,
                                    "Portalconfig.nested"
                                ]
                            ],
                            default: {flo: 3},
                            description: "Beschreibungstext",
                            expert: false
                        },
                        nesteds: {
                            name: "nesteds",
                            label: "Nesteds",
                            required: false,
                            deprecated: false,
                            types: [
                                [
                                    BasicFieldTypes.array,
                                    [
                                        BasicFieldTypes.object,
                                        "Portalconfig.nested"
                                    ]
                                ]
                            ],
                            description: "Beschreibungstext",
                            expert: false
                        },
                        k: {
                            name: "k",
                            label: "K",
                            required: false,
                            deprecated: false,
                            types: [
                                [
                                    BasicFieldTypes.object,
                                    "crossreference"
                                ]/* ,
-                               [ NOTE this snippet is correct depending on result of what inheritance should be; see annotation in resolveInheritance.js
-                                    BasicFieldTypes.object,
-                                    "Portalconfig.nested"
                                ]*/
                            ],
                            description: "asdf",
                            expert: false
                        },
                        __mpadminkey_html20: "<p><strong>Beispiel objektarray:</strong></p>",
                        __mpadminkey_html21: jsonCodeBlock
                    },
                    outgoingOrder: [
                        "__mpadminkey_html0",
                        "__mpadminkey_html1",
                        "__mpadminkey_html2",
                        "__mpadminkey_html3",
                        "__mpadminkey_html4",
                        "textEintrag",
                        "textListe",
                        "boolscher",
                        "nummer",
                        "nummerListe",
                        "schweben",
                        "vollzahl",
                        "koordinate",
                        "ausdehnung",
                        "abgezaehlt",
                        "multiAbgezaehlt",
                        "multiTypeEntry",
                        "nested",
                        "nesteds",
                        "k",
                        "__mpadminkey_html20",
                        "__mpadminkey_html21"
                    ],
                    chapterKey: "Portalconfig"
                },
                "Portalconfig.nested": {
                    name: "nested",
                    label: "Nested",
                    dataTocLabel: "Portal Config Nested",
                    path: ["Portalconfig"],
                    h: 3,
                    inherits: "crossreference",
                    outgoing: {
                        __mpadminkey_html0: "<p>Imports all table rows of crossreference.</p>",
                        flo: {
                            name: "flo",
                            label: "Flo",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.float],
                            default: 5.5,
                            description: "Beschreibungstext"
                        },
                        int: {
                            name: "int",
                            label: "Int",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.integer],
                            description: "Beschreibungstext"
                        },
                        textEintrag: {
                            name: "textEintrag",
                            label: "Text Eintrag",
                            required: true,
                            deprecated: false,
                            types: [BasicFieldTypes.string],
                            default: "alphabet",
                            description: "Beschreibungstext"
                        }
                    },
                    "outgoingOrder": [
                        "__mpadminkey_html0",
                        "flo",
                        "int",
                        "textEintrag"
                    ],
                    "chapterKey": "Portalconfig.nested"
                },
                "crossreference": {
                    name: "crossreference",
                    label: "Crossreference",
                    dataTocLabel: "Cross Reference",
                    path: [],
                    h: 2,
                    inherits: "Datatypes.inheritance",
                    outgoing: {
                        __mpadminkey_html0: "<p>Imports all table rows of Datatypes.inheritance.</p>",
                        flo: {
                            name: "flo",
                            label: "Flo",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.float],
                            default: 5.5,
                            description: "Beschreibungstext"
                        },
                        int: {
                            name: "int",
                            label: "Int",
                            required: false,
                            deprecated: false,
                            types: [BasicFieldTypes.integer],
                            description: "Beschreibungstext"
                        }
                    },
                    outgoingOrder: [
                        "__mpadminkey_html0",
                        "flo",
                        "int"
                    ],
                    chapterKey: "crossreference"
                },
                "Datatypes": {
                    chapterKey: "Datatypes",
                    dataTocLabel: null,
                    h: 2,
                    inherits: null,
                    label: "Datatypes",
                    name: "Datatypes",
                    outgoing: {},
                    outgoingOrder: [],
                    path: []
                },
                "Datatypes.inheritance": {
                    name: "inheritance",
                    label: "Inheritance",
                    dataTocLabel: null,
                    path: ["Datatypes"],
                    h: 3,
                    inherits: null,
                    outgoing: {
                        flo: {
                        name: "flo",
                        label: "Flo",
                        required: false,
                        deprecated: false,
                        types: [BasicFieldTypes.float],
                        default: 5.5,
                        description: "Beschreibungstext"
                    }
                    },
                    outgoingOrder: ["flo"],
                    chapterKey: "Datatypes.inheritance"
                }
            }
        });
    });
});
