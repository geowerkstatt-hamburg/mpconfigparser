import MarkdownIt from "markdown-it";

import parseCell from "parseMarkdown/parseCell";
import {createGraph, createEdge} from "parseMarkdown/graph";

const md = new MarkdownIt();

describe("parseMarkdown/parseCell", function () {
    it("parses name to name and label", function () {
        const graph = createGraph(),
            edge = createEdge();

        parseCell(graph, edge, "name", {content: "fieldName"}, {}, md);
        expect(edge.name).toEqual("fieldName");
        expect(edge.label).toEqual("Field Name");
        expect(graph.warnings).toHaveLength(0);
    });

    it("parses required field", function () {
        const graph = createGraph(),
            edge = createEdge();

        parseCell(graph, edge, "required", {content: "ja"}, {}, md);
        expect(edge.required).toBe(true);

        parseCell(graph, edge, "required", {content: "nein"}, {}, md);
        expect(edge.required).toBe(false);

        expect(graph.warnings).toHaveLength(0);
        parseCell(graph, edge, "required", {content: "hanebüchen"}, {}, md);
        expect(edge.required).toBe(false);
        expect(graph.warnings).toHaveLength(1);
    });

    it("parses types field", function () {
        const graph = createGraph(),
            edge = createEdge();

        // detailed type parsing tests in getFieldType.spec.js
        // this merely tests if getFieldType is used correctly (mock implementation?)
        parseCell(graph, edge, "types", {content: "String"}, {}, md);
        expect(edge.types).toEqual([String]);
        parseCell(graph, edge, "types", {content: "[victory](#pathto)"}, {definedKeys: {}, path: ["path", "to"]}, md);
        expect(edge.types).toEqual([[Object, "path.to.victory"]]);
        parseCell(graph, edge, "types", {content: "[victory](#pathto)"}, {definedKeys: {"TYPE:VICTORY": "veni.vidi.vici"}, path: ["path", "to"]}, md);
        expect(edge.types).toEqual([[Object, "veni.vidi.vici"]]);
        expect(graph.warnings).toHaveLength(0);
    });

    it("parses default value", function () {
        const graph = createGraph(),
            edge = createEdge();

        parseCell(graph, edge, "default", {content: "5"}, {}, md);
        expect(edge.default).toEqual(5);
        expect(graph.warnings).toHaveLength(0);
    });

    it("parses description to HTML string", function () {
        const graph = createGraph(),
            edge = createEdge();

        parseCell(graph, edge, "description", {content: "This is *a little* emphasized."}, {}, md);
        expect(edge.description).toEqual("This is <em>a little</em> emphasized.");
        expect(graph.warnings).toHaveLength(0);
    });

    it("parses expert value", function () {
        const graph = createGraph(),
            edge = createEdge();

        parseCell(graph, edge, "expert", {content: "true"}, {}, md);
        expect(edge.expert).toEqual(true);
        expect(graph.warnings).toHaveLength(0);
    });

    it("logs warnings for unknown headers", function () {
        const graph = createGraph(),
            edge = createEdge();

        parseCell(graph, edge, "boing", {content: "5"}, {}, md);
        expect(edge).toEqual({});
        expect(graph.warnings).toHaveLength(1);
    });
});
