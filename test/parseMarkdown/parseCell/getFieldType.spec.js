import {BasicFieldTypes} from "parseMarkdown/definitions";
import getFieldType from "parseMarkdown/parseCell/getFieldType";

describe("parseMarkdown/parseCell/getFieldType", function () {
    it("reads basic field types correctly", function () {
        expect(getFieldType("Integer", {})).toEqual([BasicFieldTypes.integer]);
        expect(getFieldType("Float", {})).toEqual([BasicFieldTypes.float]);
        expect(getFieldType("Number", {})).toEqual([BasicFieldTypes.number]);
        expect(getFieldType("String", {})).toEqual([BasicFieldTypes.string]);
        expect(getFieldType("Coordinate", {})).toEqual([BasicFieldTypes.coordinate]);
        expect(getFieldType("Extent", {})).toEqual([BasicFieldTypes.extent]);
        // while basic field types, these may not explicitly occur
        expect(getFieldType("Object", {warnings: []})).toEqual([]);
        expect(getFieldType("Array", {warnings: []})).toEqual([]);
    });

    it("reads multi-type fields correctly", function () {
        expect(getFieldType("Integer/Float", {})).toEqual([BasicFieldTypes.integer, BasicFieldTypes.float]);
        expect(getFieldType("Float/Number", {})).toEqual([BasicFieldTypes.float, BasicFieldTypes.number]);
        expect(getFieldType("Number/String/Object", {warnings: []})).toEqual([
            BasicFieldTypes.number,
            BasicFieldTypes.string
        ]);
    });

    it("reads enum fields correctly", function () {
        expect(getFieldType("enum[1, 2, 3]", {})).toEqual([
            [
                BasicFieldTypes.enum,
                [
                    1,
                    2,
                    3
                ]
            ]
        ]);

        expect(getFieldType("enum[\"optionA\", \"optionB\", \"optionC\"]", {})).toEqual([
            [
                BasicFieldTypes.enum,
                [
                    "optionA",
                    "optionB",
                    "optionC"
                ]
            ]
        ]);
    });

    it("reads multi-choice enum correctly", function () {
        expect(getFieldType("enum[1, 2][]", {})).toEqual([
            [
                Array,
                [
                    BasicFieldTypes.enum,
                    [
                        1,
                        2
                    ]
                ]
            ]
        ]);
    });

    it("reads typed array types correctly", function () {
        expect(getFieldType("Integer[]", {})).toEqual([[BasicFieldTypes.array, BasicFieldTypes.integer]]);
        expect(getFieldType("Float[]", {})).toEqual([[BasicFieldTypes.array, BasicFieldTypes.float]]);
    });

    it("reads typed array types and alternatives correctly", function () {
        expect(getFieldType("Integer[]/Integer", {})).toEqual([[BasicFieldTypes.array, BasicFieldTypes.integer], BasicFieldTypes.integer]);
        expect(getFieldType("Object[]/Boolean", {warnings: []})).toEqual([BasicFieldTypes.boolean]);
    });

    it("reads object links correctly", function () {
        const params = {
            definedKeys: {
                "TYPE:SNAIL": "deep.in.the.jungle.snail"
            },
            path: [
                "deep",
                "in",
                "the",
                "forest"
            ]
        };

        expect(getFieldType("[mail](#deepintheforestmail)", params)).toEqual([[BasicFieldTypes.object, "deep.in.the.forest.mail"]]);
        expect(getFieldType("[snail](#deepinthejunglesnail)", params)).toEqual([[BasicFieldTypes.object, "deep.in.the.jungle.snail"]]);
    });

    it("reads root object links correctly", function () {
        const params = {
            definedKeys: {"TYPE:SNAIL": "something"},
            path: []
        };

        expect(getFieldType("[mail](#mail)", params)).toEqual([[BasicFieldTypes.object, "mail"]]);
        expect(getFieldType("[snail](#something)", params)).toEqual([[BasicFieldTypes.object, "something"]]);
    });

    it("reads multi-object links correctly", function () {
        const params = {
            definedKeys: {
                "TYPE:SNAIL": "deep.in.the.jungle.snail"
            },
            path: [
                "deep",
                "in",
                "the",
                "forest"
            ]
        };

        expect(getFieldType("[mail](#deepintheforestmail)[]", params)).toEqual([[Array, [BasicFieldTypes.object, "deep.in.the.forest.mail"]]]);
        expect(getFieldType("[snail](#deepinthejunglesnail)[]", params)).toEqual([[Array, [BasicFieldTypes.object, "deep.in.the.jungle.snail"]]]);
    });

    it("reads object (array) alternatives correctly", function () {
        const params = {
                definedKeys: {
                    "TYPE:SNAIL": "deep.in.the.jungle.snail"
                },
                path: [
                    "deep",
                    "in",
                    "the",
                    "forest"
                ]
            },
            type = "[mail](#deepintheforestmail)[]/[snail](#deepinthejunglesnail)";

        expect(getFieldType(type, params))
            .toEqual([[Array, [BasicFieldTypes.object, "deep.in.the.forest.mail"]], [BasicFieldTypes.object, "deep.in.the.jungle.snail"]]);
    });

    it("reads linked basic field types correctly", function () {
        expect(getFieldType("[Coordinate](#something)", {})).toEqual([BasicFieldTypes.coordinate]);
        expect(getFieldType("[Extent](#something)", {})).toEqual([BasicFieldTypes.extent]);
    });

    it("skips broken information and logs warnings", function () {
        let warnings;

        warnings = [];
        expect(getFieldType("Integer/Flöat/Object", {warnings})).toEqual([BasicFieldTypes.integer]);
        expect(warnings).toHaveLength(2);

        warnings = [];
        expect(getFieldType("/", {warnings})).toEqual([]);
        expect(warnings).toHaveLength(3);

        warnings = [];
        expect(getFieldType("enum[1,\"2, 3]/String", {warnings})).toEqual([BasicFieldTypes.string]);
        expect(warnings).toHaveLength(1);

        warnings = [];
        expect(getFieldType("Ran/Tan/Plan/String", {warnings})).toEqual([BasicFieldTypes.string]);
        expect(warnings).toHaveLength(3);

        warnings = [];
        expect(getFieldType("Ran/Tan/Plan", {warnings})).toEqual([]);
        expect(warnings).toHaveLength(4);
    });
});
