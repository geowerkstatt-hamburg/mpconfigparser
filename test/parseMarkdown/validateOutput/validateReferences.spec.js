import validateReferences from "parseMarkdown/validateOutput/validateReferences";

describe("parseMarkdown/validateOutput/validateReferences", function () {
    it("goes through valid structures and checks all found entries on checklist", function () {
        const graph = {
                warnings: [],
                chapterOrder: ["a.b.c"],
                nodes: {
                    "a.b.c": {
                        name: "c",
                        label: "C",
                        path: ["a", "b"],
                        h: 2,
                        inherits: "x.a",
                        outgoing: {d: {
                            name: "d",
                            label: "D",
                            required: false,
                            types: [[Object, "a.b.c.d"]],
                            description: "Some object"
                        }},
                        outgoingOrder: ["d"],
                        chapterKey: "a.b.c"
                    },
                    "a.b.c.d": {
                        name: "d",
                        label: "D",
                        path: [
                            "a",
                            "b",
                            "c"
                        ],
                        h: 3,
                        outgoing: {},
                        outgoingOrder: [],
                        chapterKey: "a.b.c"
                    },
                    "x.a": {
                        name: "a",
                        label: "A",
                        path: ["x", "a"],
                        h: 4,
                        outgoing: {},
                        outgoingOrder: [],
                        chapterKey: "a.b.c"
                    }
                }
            },
            checklist = {
                "a.b.c": false,
                "doesnt.exist": false,
                "a.b.c.d": false,
                "x.a": false
            };

        validateReferences(graph, checklist);
        expect(graph.warnings).toHaveLength(0);
        expect(checklist["a.b.c"]).toBe(true);
        expect(checklist["a.b.c.d"]).toBe(true);
        expect(checklist["x.a"]).toBe(true);
        expect(checklist["doesnt.exist"]).toBe(false);
    });

    it("logs warning for missing elements", function () {
        const graph = {
                warnings: [],
                chapterOrder: ["a.b.c", "doesnt.exist.a"],
                nodes: {
                    "a.b.c": {
                        name: "c",
                        label: "C",
                        path: ["a", "b"],
                        h: 2,
                        inherits: "x.a",
                        outgoing: {
                            d: {
                                name: "d",
                                label: "D",
                                required: false,
                                types: [[Object, "a.b.c.d"]],
                                description: "Some object"
                            },
                            e: {
                                name: "e",
                                label: "E",
                                required: false,
                                types: [[Object, "doesnt.exist.b"]],
                                description: "Another object"
                            }},
                        outgoingOrder: ["d"],
                        chapterKey: "a.b.c"
                    },
                    "a.b.c.d": {
                        name: "d",
                        label: "D",
                        path: [
                            "a",
                            "b",
                            "c"
                        ],
                        h: 3,
                        outgoing: {},
                        outgoingOrder: [],
                        chapterKey: "a.b.c"
                    }
                }
            };

        validateReferences(graph, {});
        expect(graph.warnings).toHaveLength(3);
    });
});
