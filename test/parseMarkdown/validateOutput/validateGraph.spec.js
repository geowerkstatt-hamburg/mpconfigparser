import validateGraph, {makeChecklist} from "parseMarkdown/validateOutput/validateGraph";

describe("parseMarkdown/validateOutput/validateGraph", function () {
    it("notes unreached nodes according to checklist", function () {
        const graph = {
            nodes: {
                asdf: {},
                sdfa: {},
                dfas: {},
                fasd: {},
                sadf: {},
                ace: {}
            },
            chapterOrder: [],
            warnings: []
        },
            checklist = makeChecklist(graph);

        validateGraph(graph, checklist);
        expect(graph.warnings).toHaveLength(6);

        graph.warnings = [];
        checklist.asdf = true;
        checklist.dfas = true;
        checklist.sadf = true;
        validateGraph(graph, checklist);
        expect(graph.warnings).toHaveLength(3);
    });
});
