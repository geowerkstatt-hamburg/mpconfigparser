import {BasicFieldTypes} from "parseMarkdown/definitions";
import validateEdges from "parseMarkdown/validateOutput/validateEdges";

describe("parseMarkdown/validateOutput/validateEdges", function () {
    it("does not warn on valid edges", function () {
        const graph = {
            warnings: [],
            chapterOrder: [],
            nodes: {
                "a": {
                    name: "a",
                    label: "A",
                    path: [],
                    h: 3,
                    outgoing: {b: {
                        name: "b",
                        label: "B",
                        required: true,
                        description: "A thing",
                        types: [BasicFieldTypes.string]
                    }},
                    chapterKey: "a"
                }
            }
        };

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(0);
    });

    it("warns on missing required properties", function () {
        const graph = {
            warnings: [],
            chapterOrder: [],
            nodes: {
                "a": {
                    name: "a",
                    label: "A",
                    path: [],
                    h: 3,
                    outgoing: {b: {}},
                    chapterKey: "a"
                }
            }
        };

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(5);
    });

    it("detects primitive type default value correctness", function () {
        const graph = {
            warnings: [],
            chapterOrder: [],
            nodes: {
                "a": {
                    name: "a",
                    label: "A",
                    path: [],
                    h: 3,
                    outgoing: {
                        b: {
                            name: "b",
                            label: "B",
                            required: true,
                            description: "A thing",
                            types: [BasicFieldTypes.number, BasicFieldTypes.string],
                            def: "a default",
                            expert: false
                        },
                        c: {
                            name: "c",
                            label: "C",
                            required: true,
                            description: "A thing",
                            types: [
                                BasicFieldTypes.string,
                                BasicFieldTypes.number,
                                BasicFieldTypes.boolean
                            ],
                            def: true,
                            expert: true
                        }
                    },
                    chapterKey: "a"
                }
            }
        };

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(0);

        graph.nodes.a.outgoing.c.types = [BasicFieldTypes.string];

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(1);
    });

    it("detects self-defined type default value correctness", function () {
        const graph = {
            warnings: [],
            chapterOrder: [],
            nodes: {
                "a": {
                    name: "a",
                    label: "A",
                    path: [],
                    h: 3,
                    outgoing: {
                        b: {
                            name: "b",
                            label: "B",
                            required: true,
                            description: "A thing",
                            types: [BasicFieldTypes.integer, BasicFieldTypes.float],
                            def: 5.555,
                            expert: false
                        },
                        c: {
                            name: "c",
                            label: "C",
                            required: true,
                            description: "A thing",
                            types: [
                                BasicFieldTypes.extent,
                                BasicFieldTypes.coordinate
                            ],
                            def: [1, 2],
                            expert: true
                        },
                        d: {
                            name: "d",
                            label: "D",
                            required: true,
                            description: "A thing",
                            types: [
                                [
                                    BasicFieldTypes.enum,
                                    ["A", "B"]
                                ]
                            ],
                            def: "A",
                            expert: false
                        }
                    },
                    chapterKey: "a"
                }
            }
        };

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(0);

        graph.warnings = [];
        graph.nodes.a.outgoing.b.types = [BasicFieldTypes.integer];

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(1);

        graph.warnings = [];
        graph.nodes.a.outgoing.c.types = [BasicFieldTypes.extent];
        validateEdges(graph);
        expect(graph.warnings).toHaveLength(2);

        graph.warnings = [];
        graph.nodes.a.outgoing.d.def = "C";
        validateEdges(graph);
        expect(graph.warnings).toHaveLength(3);
    });

    it("detects array type default value correctness", function () {
        const graph = {
            warnings: [],
            chapterOrder: [],
            nodes: {
                "a": {
                    name: "a",
                    label: "A",
                    path: [],
                    h: 3,
                    outgoing: {
                        b: {
                            name: "b",
                            label: "B",
                            required: true,
                            description: "A thing",
                            types: [
                                [
                                    Array,
                                    BasicFieldTypes.float
                                ]
                            ],
                            def: [5.5555],
                            expert: true
                        }
                    },
                    chapterKey: "a"
                }
            }
        };

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(0);

        graph.warnings = [];
        graph.nodes.a.outgoing.b.types = [[Array, BasicFieldTypes.integer]];

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(1);
    });

    it("detects object reference type default value correctness", function () {
        const graph = {
            warnings: [],
            chapterOrder: [],
            nodes: {
                "a": {
                    name: "a",
                    label: "A",
                    path: [],
                    h: 3,
                    outgoing: {
                        b: {
                            name: "b",
                            label: "B",
                            required: true,
                            description: "A thing",
                            types: [
                                [
                                    Object,
                                    "b"
                                ]
                            ],
                            def: {c: "fasd"},
                            expert: false
                        }
                    },
                    chapterKey: "a"
                },
                "b": {
                    name: "b",
                    label: "B",
                    path: [],
                    h: 3,
                    outgoing: {
                        c: {
                            name: "c",
                            label: "C",
                            required: true,
                            description: "A thing",
                            types: [BasicFieldTypes.string],
                            def: "asdf",
                            expert: true
                        }
                    }
                }
            }
        };

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(0);

        graph.nodes.a.outgoing.b.def = {};

        validateEdges(graph);
        expect(graph.warnings).toHaveLength(2);

        graph.nodes.a.outgoing.b.def = {unknownKey: 5};

        graph.warnings = [];
        validateEdges(graph);
        expect(graph.warnings).toHaveLength(1);
    });
});
