import validateNodes from "parseMarkdown/validateOutput/validateNodes";

describe("parseMarkdown/validateOutput/validateNodes", function () {
    it("detects malformed chapter depths", function () {
        const graph = {
            warnings: [],
            nodes: {
                a: {
                    name: "a",
                    path: [],
                    h: 0,
                    outgoing: {d: "text", e: "text"},
                    outgoingOrder: ["d", "e"],
                    chapterKey: "a"
                },
                b: {
                    name: "b",
                    path: [],
                    h: 2,
                    outgoing: {d: "text", e: "text"},
                    outgoingOrder: ["d", "e"],
                    chapterKey: "b"
                },
                c: {
                    name: "c",
                    path: [],
                    h: 2,
                    outgoing: {d: "text", e: "text"},
                    outgoingOrder: ["d", "e"],
                    chapterKey: "c"
                },
                d: {
                    name: "d",
                    path: [],
                    h: 7,
                    outgoing: {d: "text", e: "text"},
                    outgoingOrder: ["d", "e"],
                    chapterKey: "d"
                },
                e: {
                    name: "e",
                    path: [],
                    h: "orse",
                    outgoing: {d: "text", e: "text"},
                    outgoingOrder: ["d", "e"],
                    chapterKey: "e"
                }
            }
        };

        validateNodes(graph);
        expect(graph.warnings).toHaveLength(3);
    });

    it("detects naming inconsistencies", function () {
        const graph = {
            warnings: [],
            nodes: {
                "a.b": {
                    name: "b",
                    path: ["x"],
                    h: 2,
                    outgoing: {d: "text", e: "text"},
                    outgoingOrder: ["d", "e"],
                    chapterKey: "a.b"
                }
            }
        };

        validateNodes(graph);
        expect(graph.warnings).toHaveLength(1);
    });

    it("detects outgoing inconsistencies", function () {
        const graph = {
            warnings: [],
            nodes: {
                "a.b": {
                    name: "b",
                    path: ["a"],
                    h: 2,
                    outgoing: {e: "text"},
                    outgoingOrder: ["d", "e"],
                    chapterKey: "a.b"
                },
                "a.b.c": {
                    name: "c",
                    path: ["a", "b"],
                    h: 2,
                    outgoing: {a: "text", e: "e"},
                    outgoingOrder: ["a"],
                    chapterKey: "a.b.c"
                },
                "a.b.d": {
                    name: "d",
                    path: ["a", "b"],
                    h: 2,
                    outgoing: {a: "text", e: "e"},
                    outgoingOrder: ["a", "b"],
                    chapterKey: "a.b.d"
                }
            }
        };

        validateNodes(graph);
        expect(graph.warnings).toHaveLength(3);
    });
});
