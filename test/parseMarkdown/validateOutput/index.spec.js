import validate from "parseMarkdown/validateOutput";
import {BasicFieldTypes} from "parseMarkdown/definitions";
import {createGraph, addNode, createNode, addEdge} from "parseMarkdown/graph";

describe("parseMarkdown/validateOutput/validateReferences", function () {
    it("does not generate warnings for well-formed graph", function () {
        const graph = createGraph();
        addNode(graph, createNode({chapterKey: "a", h: 2}));
        addNode(graph, createNode({chapterKey: "a.b.c", h: 3}));
        addNode(graph, createNode({chapterKey: "x.y.z", h: 3}));
        addNode(graph, createNode({chapterKey: "a.b.c.d", h: 4}));
        addEdge(graph.nodes.a, {
            name: "a",
            label: "A",
            required: true,
            description: "A thing",
            types: [BasicFieldTypes.string, [Object, "a.b.c"]],
            def: "",
            expert: false
        });
        addEdge(graph.nodes["a.b.c"], {
            name: "d",
            label: "D",
            required: true,
            description: "A thing",
            types: [[Object, "a.b.c.d"], [Object, "x.y.z"]],
            def: {f: "testReference"},
            expert: false
        });
        addEdge(graph.nodes["a.b.c.d"], {
            name: "e",
            label: "E",
            required: true,
            description: "A thing",
            types: [BasicFieldTypes.string],
            expert: true
        });
        addEdge(graph.nodes["x.y.z"], {
            name: "f",
            label: "F",
            required: true,
            description: "A thing",
            types: [BasicFieldTypes.string],
            expert: false
        });

        validate(graph);

        expect(graph.warnings).toHaveLength(0);
    });

    it("warns for disconnected nodes", function () {
        const graph = createGraph();
        addNode(graph, createNode({chapterKey: "a.b.c", h: 3}));
        addEdge(graph.nodes["a.b.c"], {
            name: "d",
            label: "D",
            required: true,
            description: "A thing",
            types: [String],
            def: "",
            expert: false
        });

        validate(graph);

        expect(graph.warnings).toHaveLength(1);
    });

    it("warns for empty nodes", function () {
        const graph = createGraph();
        addNode(graph, createNode({chapterKey: "a", h: 2}));

        validate(graph);

        expect(graph.warnings).toHaveLength(1);
    });
});
