# About Markdown Parsing

Some pointers about what's going on in this folder.

## Task at Hand

The masterportal is controlled by a configuration file (config.json) that describes available tools, layers, ...

For this configuration file, a markdown file exists that documents structure and contents. From this markdown file, a form shall be generated to ease the creation of such configuration files. *This folder handles parsing the markdown file to a graph that can be used to do that.*

## markdown-it

Normally a parser that translates markdown to HTML. Since its sub-functions are open for use, we use those that can be used.

* We use markdown-it as
    * tokenizer: creates a token list from markdown (.parse)
    * resolver: links are used to model inheritance and cross-references (side-effect of .parse)
    * parser: all elements not relevant to data structure are parsed with (.renderInline) to create the final HTML

See [markdown-it documentation](https://markdown-it.github.io/markdown-it/) for more functions and function details.

## documentation source

The source document is at [LGV's bitbucket repository](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/stable-candidate/doc/config.json.md). Mind that this links to the dev branch, but in the future, various tags of the stable branch are to be used.

See `markdown-definition.md` in the project root for details about the markdown file's contents.

## generated structure

The documentation source is translated to a directed circle-free graph modelling a) the configuration file b) inheritance between chapters. Check the graph folder on how exactly the generated graph looks like.
