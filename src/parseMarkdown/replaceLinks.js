const documentationTarget = "_config_documentation",
    fileTarget = "_file_target",
    externalTarget = "_blank",
    baseUrl = "https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/stable-candidate/doc/";

/**
 * Replaces anchors with anchored links to the documentation,
 * replaces other links to neighbouring files,
 * sets target for outgoing links to avoid accidental closing of forms.
 * @param {*} string may or may not contain html links
 * @returns {string} string with replaced links
 */
export default function replaceLinks (string) {
    // anchors to json.md
    let ret = string.replace(/<a href="#/g, `<a target="${documentationTarget}" href="${baseUrl}config.json.md#`);
    // open external links in new tab
    if ((/<a href="http/).test(ret)) {
        ret = ret.replace(/<a href="http/g, `<a target="${externalTarget}" href="http`);
    }
    if ((/<a href="www/).test(ret)) {
        ret = ret.replace(/<a href="www/g, `<a target="${externalTarget}" href="www`);
    }
    // expect other links to be neighbours to config.json.md
    if ((/<a href="/).test(ret)) {
        ret = ret.replace(/<a href="/g, `<a target="${fileTarget}" href="${baseUrl}`);
    }

    return ret;
}

export {
    documentationTarget,
    fileTarget,
    externalTarget,
    baseUrl
};
