import {getReferencedNodeKeys} from "./graph";
// import {BasicFieldTypes} from "./definitions";

/**
 * Interprets field inheritance for a node.
 * First does all child nodes to correctly handle inheritance chains.
 * @param {Graph} graph graph
 * @param {string} key node chapterKey to work on
 * @param {object.<string, boolean>} visited registry of interpreted chapters
 * @returns {void}
 */
function inheritFields (graph, key, visited) {
    if (visited[key]) {
        // visited node before
        return;
    }

    // only do this once per node - initially since inheritance may not have circles
    visited[key] = true;

    // by going through all references, whole reachable tree does inheritance
    const node = graph.nodes[key];

    if (node) {
        const referencedNodeKeys = getReferencedNodeKeys(node);

        // do it before inheriting (sinks first, going back to start nodes)
        referencedNodeKeys.forEach(k => inheritFields(graph, k, visited));

        if (node.inherits) {
            // make child inherit before doing it for this node to get the completed field set
            inheritFields(graph, node.inherits, visited);

            const parent = graph.nodes[node.inherits];

            // inject parent properties right before first own property
            let injectionIndex;
            if (!node.outgoingOrder.some((e, i) => {
                injectionIndex = i;
                return typeof node.outgoing[e] === "object";
            })) {
                // if no own properties, 1. what are you doing, 2. start at 0
                injectionIndex = 0;
            }

            parent.outgoingOrder.forEach(edgeName => {
                // only inherit keys 1. that model inputs 2. are not overridden by own keys
                if (typeof parent.outgoing[edgeName] !== "string" && !node.outgoing[edgeName]) {
                    node.outgoing[edgeName] = parent.outgoing[edgeName];
                    node.outgoingOrder.splice(injectionIndex++, 0, edgeName);
                }
            });
        }
    }
}

/** NOTE see todo block below on why this is commented out
 * Extends allowed types to satisfy inheritance requirements.
 * That is, if in some spot A is allowed, and B inherits from A, B should be allowed, too.
 * @param {Graph} graph graph to work on
 * @returns {void}
/
function extendTypes (graph) {
    // constructing object.<base, heir[]> with all heirs, including transitive heirs
    const transitiveInheritRegistry = Object.values(graph.nodes).reduce((acc, node) => {
        // if note inherits and didn't go through that inheritance (sub-)tree yet
        if (node.inherits && !(acc[node.chapterKey])) {
            const heirs = [];
            let curr = node,
                base;

            // step through inheritance chain; take note of every partial completion
            do {
                heirs.push(curr.chapterKey);
                base = curr.inherits;
                acc[base] = acc[base] ? [...new Set(acc[base].concat(heirs))] : [...heirs];
                curr = graph.nodes[curr.inherits];
            } while (curr.inherits);
        }
        return acc;
    }, {});

    // for each node's ...
    Object.values(graph.nodes).forEach(node => {
        // ... typed edge's ...
        Object.values(node.outgoing).forEach(edge => {
            const newTypes = [];
            if (typeof edge === "object") {
                // ... type, ...
                edge.types.forEach(type => {
                    // ... if it's an object([]?), also add all heirs([]?) of object as type
                    if (Array.isArray(type) && type[0] === BasicFieldTypes.object) {
                        if (transitiveInheritRegistry[type[1]]) {
                            transitiveInheritRegistry[type[1]].forEach(heir => {
                                newTypes.push([BasicFieldTypes.object, heir]);
                            });
                        }
                    }
                    if (Array.isArray(type) && type[0] === BasicFieldTypes.array && type[1][0] === BasicFieldTypes.object) {
                        if (transitiveInheritRegistry[type[1][1]]) {
                            transitiveInheritRegistry[type[1][1]].forEach(heir => {
                                newTypes.push([Array, [BasicFieldTypes.object, heir]]);
                            });
                        }
                    }
                });
                edge.types = edge.types.concat(newTypes);
            }
        });
    });
}
// */

/**
 * The graph nodes have inheritance modelled as link to another node.
 * Since inheritance is purely field-based and no effects/changes take place
 * during run-time, all inheritance can be resolved to create extended nodes.
 * The field edges will be placed right prior to all other field edges.
 * No other edges are inherited, i.e. no description texts and so on.
 * @param {Graph} graph graph to resolve inheritance in
 * @returns {void}
 */
export default function (graph) {
    const done = {}; // keeping track of processed nodes

    graph.chapterOrder.forEach(entryPoint => inheritFields(graph, entryPoint, done));

    /*
     * TODO while extending types is semantically correct, config.json.md is not written so that
     * it makes sense in the frontend: many tools simply link to "tool", and since some special
     * tools have their own object definitions inheriting from "tool", all these special tools
     * can be used for the generic link.
     *
     * Example:
     * 1. "einwohnerabfrage" is a "tool"
     * 2. "compareFeatures" is a "compareFeatures" definition that inherits from tool
     * -> "einwohnerabfrage" can now either be "tool" or the implementing class "compareFeatures"
     * "einwohnerabfrage" should obviously not be a "compareFeatures" since it's a specialized tool
     *
     * Either we're to reinterpret what inheritance means, or this has to be modelled differently.
     * Both cases hurt in different situations.
     * NOTE This issue has been posted for discussion to ticket MPADMIN-5.
     */
    // extendTypes(graph);
}
