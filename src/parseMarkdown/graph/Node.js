import camelToTitle from "../camelToTitle";

/**
 * A node resembles a chapter, which in turn equals an object description.
 * Such an object description may inherit from other object descriptions.
 * Such a node has outgoing edges to other nodes (nested objects) and sinks (final input fields and various display elements).
 * @typedef {Object} Node
 * @property {string} name name of element; for chapter x.y.z, name is z
 * @property {string} label name, but transformed from camelCase to Title Case for pretty display
 * @property {string[]} path path to element; for chapter x.y.z, [x, y] is the path
 * @property {string} chapterKey header string; for chapter x.y.z, it's x.y.z; used as global ID
 * @property {number} h chapter depth (h1-h6)
 * @property {string} [inherits] chapter key of object description to inherit from
 * @property {Object.<string, (Edge|String)>} outgoing entry name to outgoing edges to other nodes, or display strings
 * @property {string[]} outgoingOrder keys of outgoing in order of appearance in documentation
 */

/**
 * Creates a new node from minimal set of parameters.
 * @param {object} params parameter object
 * @param {string} params.chapterKey chapter key of new node as dot-separated string
 * @param {number} h chapter depth (h1-h6)
 * @param {?string} [params.inherits=null] chapter key of element to inherit from; if blank, no inheritance
 * @returns {Node} new node with configured name/path/label/inherits
 */
export function createNode ({chapterKey, h, inherits = null}) {
    let pureChapterKey = chapterKey,
        dataTocLabel = null;

    if ((/\s\{\s*data-toc-label='.+'\s*\}/g).test(chapterKey)) {
        [pureChapterKey, ...dataTocLabel] = pureChapterKey.split(" ");
        dataTocLabel = dataTocLabel.join(" ").split("'").slice(1, -1).join("'");
    }

    const path = pureChapterKey.split("."),
        name = path ? path.pop() : null,
        node = {
            name,
            dataTocLabel,
            label: name ? camelToTitle(name) : "",
            path,
            h,
            inherits,
            outgoing: {},
            outgoingOrder: [],
            chapterKey: pureChapterKey
        };

    return node;
}

/**
 * Adds an outgoing edge or string to a node.
 * @param {Node} node node to add edge to
 * @param {(Edge|string)} param either an Edge or a text node
 * @returns {(Edge|String)} returns the added element
 */
export function addEdge (node, param) {
    if (typeof param === "object") {
        node.outgoing[param.name] = param;
        node.outgoingOrder.push(param.name);
    }
    else {
        // node-unique key that shouldn't appear in config.json.md
        const key = `__mpadminkey_html${node.outgoingOrder.length}`;

        node.outgoing[key] = param;
        node.outgoingOrder.push(key);
    }

    return param;
}

/**
 * Returns all keys of nodes referenced in the outgoing edges.
 * No duplicates and no self-references are returned.
 * @param {Node} node node to get outgoing object references for
 * @param {boolean} addName if true, object is returned that holds key as "key" and name as "name"
 * @param {boolean} [expertMode=true] if false, expert edges will be skipped
 * @returns {(string[]|object)} array of keys of outgoing object references, optionally as object with "key" and "name"
 */
export function getReferencedNodeKeys (node, addName = false, expertMode = true) {
    // dedupe by using a set first
    return [
        ...Object
            .values(node.outgoing)
            .reduce((acc, outgoing) => {
                if (typeof outgoing === "object") {
                    if (expertMode || !outgoing.expert) {
                        outgoing.types.forEach(type => {
                            let target;
                            // type is object reference
                            if (Array.isArray(type) && type[0] === Object) {
                                target = type[1];
                            }
                            // type is array of referenced object
                            if (Array.isArray(type) && type[0] === Array && Array.isArray(type[1]) && type[1][0] === Object) {
                                target = type[1][1];
                            }
                            // only add edge if it's not a self-reference
                            if (target && target !== node.chapterKey) {
                                acc.add(addName ? {name: outgoing.name, key: target} : target);
                            }
                        });
                    }
                }
                return acc;
            }, new Set())
    ];
}
