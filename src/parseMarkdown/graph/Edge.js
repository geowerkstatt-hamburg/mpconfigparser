/**
 * An edge directs to another node if the type is Object or Object[].
 * In all other cases, it directs to an implicit and locally declared sink node.
 * Due to inheritance, multiple nodes may use this sink node.
 * No read/write path is defined here; since elements may be inherited from, that path has
 * to be constructed in combination with the holding node.
 * @typedef {Edge}
 * @property {string} name name of element
 * @property {string} label name, but transformed from camelCase to Title Case for pretty display
 * @property {boolean} required whether the field must be filled
 * @property {boolean} deprecated whether field is readOnly
 * @property {FieldType[]} types possible field types for this edge
 * @property {*} [def] for default; initial value for field matching type
 * @property {string} description information about field
 * @property {boolean} [expert] if true, field will only show in expert mode
 */

/**
 * Creates a blank edge.
 * @returns {Edge} incomplete edge; fill by using with parseTableRow/parseCell
 */
export function createEdge () {
    return {};
}
