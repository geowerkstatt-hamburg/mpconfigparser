/**
 * The config.json.md resembles, for this use-case, a circle-free directed graph.
 * This is the graph containing all nodes and information about order of elements.
 * @typedef {Object} Graph
 * @property {string[]} chapterOrder all h2 chapter keys in order of appearance
 * @property {Object.<string, Node>} nodes maps chapter keys to nodes
 * @property {string[]} warnings information about problems that occured during creation (debug/CI)
 */

import {containerChapters} from "../contract";

/**
 * Creates an empty graph.
 * @returns {Graph} new graph
 */
export function createGraph () {
    return {chapterOrder: [], nodes: {}, warnings: []};
}

/**
 * Adds a node to a graph as side-effect.
 * If node is h2, it will be registered as entry point to the document.
 * @param {Graph} graph graph to add node to
 * @param {Node} node node to add to graph
 * @returns {void}
 */
export function addNode (graph, node) {
    graph.nodes[node.chapterKey] = node;
    // h2 marks graph entry keys
    if (node.h === 2 && !containerChapters.includes(node.chapterKey)) {
        graph.chapterOrder.push(node.chapterKey);
    }
}
