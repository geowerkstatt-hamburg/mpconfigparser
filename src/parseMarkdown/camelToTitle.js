/**
 * Formats a camelCase string to a Title Case string.
 * @param {string} camelCase string in camel case
 * @returns {string} camelCase string translated to Title Case
 */
export default function camelToTitle (camelCase) {
    return camelCase
        .split(/([A-Z]?[(a-z|0-9)]+)/)
        .filter(s => s) // remove empty strings
        .map(s => s.charAt(0).toUpperCase() + s.slice(1))
        .join(" ");
}
