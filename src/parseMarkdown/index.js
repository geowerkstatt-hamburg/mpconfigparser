import MarkdownIt from "markdown-it";

import {createGraph} from "./graph";
import parseChapter from "./parseChapter";
import resolveInheritance from "./resolveInheritance";
import validateOutput from "./validateOutput";

import devModeFlag from "./devModeFlag";

/** tokenizer */
const md = new MarkdownIt();

/**
 * Generates a graph from the documentation markdown.
 * From it, information on how to generate form and create config file can be read.
 * Function can also be used to validate a config.json.md - returned graph holds warnings.
 * @param {string} source text of config.json.md to be used
 * @param {string} filename filename of config.json.md if given
 * @returns {Graph} generated graph
 */
export default function parseMarkdown (source, filename = "config.json.md") {
    // separate chapters by #s, but keep #s; special case where line begins with #!json in s is ignored
    const chapters = source.split(/(?=^#{1,6}(?!!json))/m),
        graph = createGraph();

    // throw away content prior to first chapter (table of contents, links, ...)
    if (chapters[0].charAt(0) !== "#") {
        chapters.shift();
    }

    // chapters must be parsed sequentially since link labels are hijacked for inheritance/reference
    // normally link label "inherit" would have a global meaning; in this hack, it has local meaning
    while (chapters.length) {
        const chapter = chapters.shift(),
            links = {/* filled with md.parse */},
            tokens = md.parse(chapter, links),
            // results in something like {INHERITS:'a.b.c', "TYPE:X":'x.y.z'}
            flatLinks = Object.keys(links.references || {}).reduce((acc, next) => {
                acc[next] = links.references[next].title;
                return acc;
            }, {}),
            // returns stop sign if content looks like it's over (~ new h1 after config.json)
            stop = parseChapter(graph, tokens, flatLinks, md);

        if (stop) {
            break;
        }
    }

    // fine to inherit now, all targets are present (unless md is broken)
    resolveInheritance(graph);

    // potentially adds (more) warnings to graph that weren't visible at compile time
    validateOutput(graph);

    // only log errors to console in dev (and test) mode
    if (devModeFlag && graph.warnings.length) {
        console.warn(`Parsing the ${filename} led to these warnings:`);
        graph.warnings.forEach(w => console.warn(w));
    }

    return graph;
}
