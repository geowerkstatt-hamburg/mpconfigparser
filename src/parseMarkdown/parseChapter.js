import {createNode, createEdge, addNode, addEdge} from "./graph";
import parseCell from "./parseCell";
import replaceLinks from "./replaceLinks";
import {headers} from "./definitions";
import he from "he";

/**
 * Markdown chapter parser adds chapter node to graph.
 * @param {Graph} graph graph to add chapter nodes to
 * @param {object[]} tokens markdown-it chapter tokens
 * @param {object} links markdown-it chapter links (hijacked for inheritance/references)
 * @param {object} md markdown parser
 * @returns {boolean} stop signal; if true, done
 */
export default function parseChapter (graph, tokens, links, md) {
    const tokenReaders = {};
    let node, // new node
        edge, // for new node's edges
        h, // current chapter level 1-6
        token, // current token
        headerCounter, // to go through columns
        list; // to add up list html

    function consumeUntilClosed (t) {
        do {
            token = tokens.shift();
            tokenReaders[t] && tokenReaders[t]();
        } while (token.consumed || token.type !== `${t}_close`);
        // when stepping out of recursive consumers, the outer should not close on the same token as the inner
        token.consumed = true;
    }

    tokenReaders.heading = () => {
        if (token.type === "inline") {
            node = createNode({chapterKey: token.content, h, inherits: links.INHERITS});
        }
    };
    tokenReaders.tr = () => {
        if (token.type === "inline") {
            parseCell(graph, edge, headers[headerCounter++], token, {definedKeys: links, path: [...node.path, node.name]}, md);
        }
    };
    tokenReaders.list = () => {
        // handling nested lists
        if (token.type === "ordered_list_open") {
            list += "<ol>";
            consumeUntilClosed("ordered_list");
            list += "</ol>";
        }
        else if (token.type === "bullet_list_open") {
            list += "<ul>";
            consumeUntilClosed("bullet_list");
            list += "</ul>";
        }
        // handling items
        else if (token.type === "inline") {
            list += `<li>${md.renderInline(token.content, {})}</li>`;
        }
    };
    tokenReaders.paragraph = () => token.type === "inline" && addEdge(node, replaceLinks(`<p>${md.renderInline(token.content, {})}</p>`));
    tokenReaders.blockquote = () => token.type === "inline" && addEdge(node, replaceLinks(`<blockquote>${md.renderInline(token.content, {})}</blockquote>`));
    tokenReaders.bullet_list = () => tokenReaders.list();
    tokenReaders.ordered_list = () => tokenReaders.list();

    while (tokens.length) {
        token = tokens.shift();
        const match = token.type.match(/_open$/),
            topic = match ? token.type.slice(0, match.index) : null;

        if (topic === "heading") {
            // e.g. "h3" to 3
            h = parseInt(token.tag[1], 10);
            consumeUntilClosed(topic);
            // if h1 found, chapter is not represented and creation can be stopped
            if (h === 1) {
                // if chapter "config.json" (with anything following, e.g. "chapter 3.0" would fit), read on; if not, stop parsing
                return !(/^config\.json/g).test(node.chapterKey);
            }
        }
        else if (typeof node === "undefined") {
            graph.warnings.push(`Received token '${token.type}' before header.`);
        }
        else if (topic === "tr") {
            edge = createEdge();
            headerCounter = 0;
            consumeUntilClosed("tr");
            addEdge(node, edge);
        }
        else if (topic === "paragraph" || topic === "thead" || topic === "blockquote") {
            consumeUntilClosed(topic);
        }
        else if (topic === "bullet_list" || topic === "ordered_list") {
            const tag = topic === "bullet_list" ? "ul" : "ol";
            list = `<${tag}>`;
            consumeUntilClosed(topic);
            list += `</${tag}>`;
            addEdge(node, list);
        }
        else if (topic === "") {
            consumeUntilClosed(topic);
        }
        else if (token.type === "fence") {
            const hasTitle = (/title=".*"/g).test(token.info);
            addEdge(node, `<figure>${hasTitle ? `<figcaption>${he.encode(token.info.split("\"").slice(1, -1).join("\""))}</figcaption>` : ""}<code>${token.content}</code></figure>`);
        }
    }

    addNode(graph, node);
    return false;
}
