/**
 * Holds blame strings that are commonly used to indicate where errors may stem from.
 */
export default {
    markdown: "This is likely to be an error within the markdown file config.json.md.",
    admintool: "This is likely to be an error within the admintool markdown parser."
};
