import doesValueMatchType from "../doesValueMatchType";
import blame from "./blame.js";

/**
 * Validates a single edge. Mostly checks whether fields were set.
 * @param {Graph} graph graph to write warnings to
 * @param {Node} node node to get path info from
 * @param {Edge} edge edge to check
 * @returns {void}
 */
function validateEdge (graph, node, edge) {
    const name = [
        ...node.path,
        node.name,
        edge.name || "???"
    ].join(".");

    if (!edge.name) {
        graph.warnings.push(`Unnamed edge '${name}'. Input can't be saved.`);
    }

    if (!edge.label) {
        graph.warnings.push(`Unlabeled edge '${name}'. Display element missing label.`);
    }

    if (typeof edge.required !== "boolean") {
        graph.warnings.push(`Edge '${name}' misses 'required' information.`);
    }

    if (!edge.description) {
        graph.warnings.push(`Edge '${name}' misses a description.`);
    }

    if (!edge.types) {
        graph.warnings.push(`Edge '${name}' has no parseable type. No display element can be generated.`);
    }
    else if (typeof edge.def !== "undefined" && !doesValueMatchType(graph, node, edge, {key: "def"})) {
        graph.warnings.push(`Edge '${name}'s default value does not match any of its types.`);
    }
}

/**
 * Checks all edges of a graph for completeness. Only typed edges are checked.
 * @param {Graph} graph graph of which edges are to be checked
 * @returns {void}
 */
export default function validateEdges (graph) {
    Object.values(graph.nodes).forEach(node => {
        Object.values(node.outgoing).forEach(edge => {
            if (typeof edge === "object") {
                validateEdge(graph, node, edge);
            }
            else if (typeof edge !== "string") {
                graph.warnings.push(`Edge of type '${typeof edge}' found in node '${node.chapterKey}'. All edges should be strings or objects. ${blame.admintool}`);
            }
        });
    });
}
