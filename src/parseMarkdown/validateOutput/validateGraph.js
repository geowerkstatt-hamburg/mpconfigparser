import blame from "./blame.js";
import {containerChapters, customTypes} from "../contract.js";

/**
 * Checks graph for completeness.
 * @param {Graph} graph graph to check
 * @param {object.<string, boolean>} checklist holds information which nodes have been visited
 * @returns {void}
 */
export default function validateGraph (graph, checklist) {
    Object.keys(graph.nodes).forEach(key => {
        if (!checklist[key] && !customTypes.includes(key) && !containerChapters.includes(key)) {
            graph.warnings.push(`The chapter '${key}' is disconnected. (Not inherited from, not linked to, not a used subclass, not an entry point.) ${blame.markdown}`);
        }
    });
    if (Object.keys(graph.nodes).length === 0) {
        graph.warnings.push("The graph has no nodes. This indicates a problem with the basic document structure. It's e.g. possible the root node \"# config.json\" was not found – was its name changed?");
    }
    if (graph.chapterOrder.some((key) => key.includes("."))) {
        graph.warnings.push(`The graph's chapter order ['${graph.chapterOrder.join("','")}'] contains chapters including dot notation, indicating an erroneously indented chapter that was supposed to be nested deeper than level 2 (##) or belongs in the Datatypes chapter.`);
    }
}

/**
 * Returns a checklist to note which nodes can be reached.
 * @param {Graph} graph graph to make checklist for
 * @returns {object.<string, boolean>} map of all node keys to false
 */
export function makeChecklist (graph) {
    return Object.values(graph.nodes).reduce((acc, next) => {
        // main chapter config.json is entry point and implicitly visitied
        acc[next.chapterKey] = next.h === 1;
        return acc;
    }, {});
}
