
import validateEdges from "./validateEdges";
import validateNodes from "./validateNodes";
import validateReferences from "./validateReferences";
import validateGraph, {makeChecklist} from "./validateGraph";

/**
 * Validates the finished graph for all details that can't be checked during transpile time.
 * E.g. whether all links work can't be checked in advance since link targets may be defined
 * later in the document. All problem descriptions will be pushed to graph.warnings.
 * @param {object} graph finished graph to check validity of
 * @returns {void}
 */
export default function validate (graph) {
    const checklist = makeChecklist(graph);

    validateEdges(graph);
    validateNodes(graph);
    validateReferences(graph, checklist);
    validateGraph(graph, checklist);
}
