import {getReferencedNodeKeys} from "../graph";
import blame from "./blame.js";

/**
 * Checks if all object/inheritance references work.
 * @param {Graph} graph graph to check
 * @param {object.<string, boolean>} checklist checklist for all visited nodes
 * @param {string} key current key to check
 * @param {string[]} [keyStack=[]] to keep track of how we got here in recursion
 * @returns {void}
 */
function validateReference (graph, checklist, key, keyStack = []) {
    const node = graph.nodes[key],
        nextKeyStack = [...keyStack, key];

    if (!node) {
        graph.warnings.push(`The chapter '${key}' is linked to, but was not found. Dangling pointer in reference chain: ${keyStack.map(s => `'${s}'`).join(" > ")}; ${blame.markdown} Please also check the spelling and character casing.` || "root (h2)");
    }
    else if (!checklist[key]) {
        checklist[key] = true;
        // inheritance check
        if (node.inherits) {
            validateReference(graph, checklist, node.inherits, nextKeyStack);
        }
        // inheritor check
        Object.values(graph.nodes).forEach(inheritorNode => {
            if (inheritorNode.inherits === key) {
                validateReference(graph, checklist, inheritorNode.chapterKey, nextKeyStack);
            }
        });
        // edge check
        const keys = getReferencedNodeKeys(node);
        keys.forEach(k => validateReference(graph, checklist, k, nextKeyStack));
    }
}

/**
 * Starting from entry point(s) to update checklist in the process.
 * @param {Graph} graph graph to check
 * @param {object.<string, boolean>} checklist checklist for all visited nodes
 * @returns {void}
 */
export default function validateReferences (graph, checklist) {
    graph.chapterOrder.forEach(key => validateReference(graph, checklist, key));
}
