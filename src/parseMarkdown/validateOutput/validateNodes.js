import blame from "./blame.js";
import {customTypes, containerChapters} from "../contract.js";

/**
 * Checks all nodes for invalid states.
 * @param {Graph} graph graph of which all nodes are to be checked
 * @returns {void}
 */
export default function validateNodes (graph) {
    Object.keys(graph.nodes).forEach(key => {
        const node = graph.nodes[key],
            out = new Set(Object.keys(node.outgoing)),
            order = new Set(node.outgoingOrder);

        /* CHECK: does node model an empty object?
         * The special chapter Datatypes (which serves as a type definition
         * container) and custom types (of which content is defined by contract)
         * are empty by design.
         */
        if (!customTypes.includes(key) && !containerChapters.includes(key) && !Object.keys(node.outgoing).filter((outgoingKey) => !outgoingKey.startsWith("__mpadminkey_")).length && !node.inherits) {
            graph.warnings.push(`Chapter '${key}' models an empty object, which is probably erroneous. If you modeled your code to require an empty object, please opt for an enum or a boolean instead.`);
        }

        // CHECK: do invalid hs exist?
        if (typeof node.h !== "number" || node.h < 1 || node.h > 6) {
            graph.warnings.push(`Chapter '${key}' with invalid chapter depth '${node.h}' added. Must be a number of 1 to 6. ${blame.admintool}`);
        }

        // CHECK: is outgoing edge model malformed?
        if (out.size !== order.size || !Array.from(out).every(order.has.bind(order))) {
            graph.warnings.push(`Chapter ${key} has outgoing edges [${[...out].toString()}](${Object.keys(node.outgoing).length}) diverging from described order [${[...order].toString()}](${node.outgoingOrder.length}). ${blame.admintool}`);
        }

        // CHECK: is path/name/chapterKey malformed?
        if ([...node.path, node.name].join(".") !== node.chapterKey) {
            graph.warnings.push(`Chapter '${key}' has malformed properties. Name: '${node.name}'; Path: '${node.path.join(".")}'; ChapterKey: '${node.chapterKey}'; ${blame.admintool}`);
        }
    });
}
