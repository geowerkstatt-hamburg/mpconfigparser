import {separators} from "../definitions";

import getSingleFieldType from "./getSingleFieldType";

/**
 * Interprets a single type cell from a table that denotes one or multiple types.
 * @param {string} s string denoting type(s)
 * @param {object} params parameter object
 * @param {object.<string, string>} [params.definedKeys={}] dictionary of pre-defined object key resolutions
 * @param {string[]} [params.path=[]] path to current row (full chapter) for entries without definedKey
 * @param {string[]} params.warnings warnings array to push warnings to
 * @returns {FieldType[]} types from FieldType; empty if none could be inferred
 */
export default function getFieldType (s, {definedKeys = {}, path = [], warnings}) {
    const parsed = s
        .split(new RegExp(separators.join("|"), "g"))
        .map(x => x.replace(/ /g, ""))
        .map(t => {
            try {
                const fieldType = getSingleFieldType(t, {definedKeys, path, warnings});
                if (!fieldType) {
                    warnings.push(`Within path '${path.join(".")}': Fragment '${t}' of type string '${s}' could not be interpreted and is skipped.`);
                }
                return fieldType;
            }
            catch (e) {
                warnings.push(`Interpreter crashed on fragment '${t}' of type string '${s}' with error '${e.message}'.`);
                return false;
            }
        })
        .filter(x => x); // remove falsy entries => partial functionality, if any is left

    if (!parsed.length) {
        warnings.push(`Type notation '${s}' has no readable type(s).`);
    }

    return parsed;
}
