import {BasicFieldTypes} from "../definitions";

const linkCheck = /^\[(.+?)\]\(#.+\)(\[\]?)?$/, // detects/destructs a(n array of) object links
    arrayCheck = /^.+\[\]$/; // detects whether a string has array suffix

/**
 * Checks the defined keys registry for a matching type.
 * @param {object} definedKeys registry of cross-reference type definitions for current chapter
 * @param {string} objectName name to search for
 * @returns {string} link if found; empty string if not
 */
function getLink (definedKeys, objectName) {
    const lookupName = `TYPE:${objectName.toUpperCase()}`;
    return definedKeys[lookupName] || "";
}

/**
 * Removes font formatting asterisks from start/end of string.
 * String may also contain a final [] that has to be saved.
 * @param {String} key string that potentially contains **'s at start/end
 * @returns {String} slim string (e.g. "**test**" -> "test")
 */
function removeFat (key) {
    const suffix = (/\*+\[\]$/).test(key) ? "[]" : "";
    return key.replace(/^\*+|\*+(\[\])?$/g, "") + suffix;
}

/**
 * Interprets a single type string to create an instance of {@see FieldType}.
 * @param {string} input type string
 * @param {object} params parameter object
 * @param {object.<string, string>} params.definedKeys dictionary of pre-defined object key resolutions
 * @param {string[]} params.path path to current row (full chapter) for entries without definedKey
 * @param {string[]} params.warnings warnings array to push warnings to
 * @returns {(FieldType|boolean)} field type, or false if none could be inferred
 */
export default function getSingleFieldType (input, {definedKeys, path, warnings}) {
    // keeping original-case for e.g. enum values
    const t = removeFat(input);
    let lowerCaseT = t.toLowerCase(),
        linkDestructured;

    // types (even BasicFieldTypes) may link to chapters; if so, retrieve display value from link
    if (linkCheck.test(t)) {
        linkDestructured = linkCheck.exec(t);
        lowerCaseT = linkDestructured[1].toLowerCase();
    }

    // types that can be looked up directly can be returned immediately
    // "array" ("object") is not directly permitted, but results from *[] notation (reference.resolution)
    if (!["array", "object"].includes(lowerCaseT) && BasicFieldTypes[lowerCaseT]) {
        return BasicFieldTypes[lowerCaseT];
    }

    // enums
    if (lowerCaseT.startsWith("enum[")) {
        const enumCloseIndex = t.indexOf("]") + 1,
            options = JSON.parse(t.substring(4, enumCloseIndex)),
            rest = t.substring(enumCloseIndex),
            enumType = [BasicFieldTypes.enum, options];

        if (rest === "[]") {
            return [Array, enumType];
        }
        if (!rest) {
            return enumType;
        }

        warnings.push(`Could not interpret enum '${t}'; suffix is invalid.`);
        return false;
    }

    // object links
    if (linkDestructured) {
        const objectName = linkDestructured[1],
            link = getLink(definedKeys, objectName) || [...path, objectName].join("."),
            objectType = [Object, link];

        if (linkDestructured[2] === "[]") {
            return [Array, objectType];
        }
        return objectType;
    }

    // arrays
    if (arrayCheck.test(t)) {
        // array notation is "type[]"
        const arrayTypeString = t.slice(0, t.indexOf("["));
        if (arrayTypeString) {
            const arrayType = getSingleFieldType(arrayTypeString, {definedKeys, path, warnings});
            if (arrayType) {
                return [Array, arrayType];
            }
        }
        warnings.push(`Array '${t}' type could not be interpreted.`);
        return false;
    }

    // layerId
    if (lowerCaseT === "layerid") {
        return BasicFieldTypes.layerId;
    }

    // restId
    if (lowerCaseT === "restid") {
        return BasicFieldTypes.restId;
    }

    // styleId
    if (lowerCaseT === "styleid") {
        return BasicFieldTypes.styleId;
    }

    return false;
}
