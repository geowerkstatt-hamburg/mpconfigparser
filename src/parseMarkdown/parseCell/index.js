import camelToTitle from "../camelToTitle";
import replaceLinks from "../replaceLinks";
import getFieldType from "./getFieldType";

const validRequiredTokens = [
    "ja",
    "yes",
    "nein",
    "no"
];

/**
 * Parses a cell value of the table definitions and writes it directly to the new graph edge.
 * @param {Graph} graph graph worked on
 * @param {object} edge edge to write to
 * @param {string} header name of current column
 * @param {object} token current inline token
 * @param {object} getTypeParams parameters required by getFieldType (except warnings)
 * @param {object} md used to parse inline text
 * @returns {void}
 */
export default function parseCell (graph, edge, header, token, getTypeParams, md) {
    switch (header) {
        case "name":
            edge.name = token.content;
            edge.label = camelToTitle(token.content);
            break;
        case "required":
            if (!validRequiredTokens.includes(token.content)) {
                graph.warnings.push(`Within tag '${edge.label}': Received '${token.content}' as value for 'required' field, but only 'ja'/'yes' and 'nein'/'no' are allowed. Falls back to 'nein'/'no'.`);
            }
            edge.required = token.content === "ja" || token.content === "yes";
            break;
        case "types":
            edge.types = getFieldType(token.content, {warnings: graph.warnings, ...getTypeParams});
            break;
        case "default":
            if (token.content) {
                try {
                    edge[header] = JSON.parse(token.content);
                }
                catch (e) {
                    graph.warnings.push(`Within tag '${edge.label}': Markdown column '${header}' value '${token.content}' could not be parsed with JSON.parse.`);
                }
            }
            break;
        case "description":
            edge.description = replaceLinks(md.renderInline(token.content, {}));
            edge.deprecated = edge.description.includes("@deprecated");
            /* config.json(.de).md should usually hold the relevant information and what to use instead.
             * Commenting this out for now, maybe a generic text like this is needed via language key later on?
             * - dse */
            // edge.description = edge.description.replace(/@deprecated\.?/g, "@deprecated: Dieses Feld wird bald nicht mehr unterstützt und kann nicht bearbeitet werden.");
            break;
        case "expert":
            if (token.content !== "true" && token.content !== "false") {
                graph.warnings.push(`Within tag '${edge.label}': Received '${token.content}' as value for 'expert' field, but only 'true' and 'false' are allowed.`);
            }
            edge.expert = token.content === "true";
            break;
        default:
            graph.warnings.push(`Withing tag '${edge.label}': Called parseCell for unknown header '${header}' on token ${JSON.stringify(token)}.`);
    }
}
