/**
 * A FieldType is either
 * * a constructor for the specific type (e.g. Number, String, ...)
 * * an array of a constructor and a string denoting meta information (e.g. [Number, "Integer"] to specialize Number, [Object, "Portalconfig.x.y"] to refer to a definition, ...)
 * * an array of two constructors, where the first creates a collection and the second the contents (e.g. [Array, Number])
 * * an array of a symbol and an array, where the array specifies a set of options for the symbol (e.g. [Symbol.for("enum"), ["optionA", "optionB"]])
 * * a symbol for a specific type (e.g. Symbol.for("extent") which has a special input element)
 * @typedef {(Constructor|[Constructor, String]|[Constructor, Constructor]|[Symbol, Array]|Symbol)} FieldType
 */

/**
 * Lookup for basic types. For complex types, {@see FieldType}.
 * NOTE If you extend this, check if you need to extend store/typeStorage.js.
 */
const BasicFieldTypes = {
        //
        // masterportal documentation types
        //
        coordinate: Symbol.for("coordinate"),
        extent: Symbol.for("extent"),
        integer: [Number, "Integer"],
        float: [Number, "Float"], // same as Number
        enum: Symbol.for("enum"), // should not be used alone, but in combination [BasicFieldTypes.enum, ["optionA", "optionB", ...]]
        layerId: Symbol.for("layerId"),
        restId: Symbol.for("restId"),
        styleId: Symbol.for("styleId"),
        //
        // normal json types
        //
        number: Number,
        boolean: Boolean,
        string: String,
        object: Object, // links to another form description - results in a sub-form
        array: Array // untyped array, typed is [Array, Constructor] {@see FieldType}
    },
    /** separators used for multiple types in config.json.md */
    separators = ["/"],
    /** headers of tables in config.json.md (columns are fixed order) */
    headers = [
        "name",
        "required",
        "types",
        "default",
        "description",
        "expert" // optional, if not set all are considered false
    ];

function isIntegerType (x) {
    return Array.isArray(x) && x[0] === BasicFieldTypes.integer[0] && x[1] === BasicFieldTypes.integer[1];
}

function isFloatType (x) {
    return Array.isArray(x) && x[0] === BasicFieldTypes.float[0] && x[1] === BasicFieldTypes.float[1];
}

export {BasicFieldTypes, separators, headers, isIntegerType, isFloatType};
