import {BasicFieldTypes, isIntegerType, isFloatType} from "./definitions";

/**
 * Returns true if value matches any of the given types. Value must be defined.
 * @param {Graph} graph full graph
 * @param {Node} node edge's node
 * @param {Edge} edge edge to evaluate
 * @param {object} vk edge field to check
 * @param {string} [vk.key] if a key is to be checked; required if vk.val is missing
 * @param {*} [vk.val] if a specific value is to be checked; required if vk.key is missing
 * @param {FieldTypes[]} [types=edge.types] overridable for recursive testing
 * @returns {boolean} true if value matches a type
 */
export default function doesValueMatchType (graph, node, edge, {key, val}, types = edge.types) {
    const value = typeof val !== "undefined" ? val : edge[key];

    // Go through all types; as soon as one matches, return true
    for (let i = 0; i < types.length; i++) {
        const type = types[i];

        //
        // primitives
        //
        if (typeof type === "function" &&
            typeof value === typeof type()) {
            return true;
        }

        //
        // self-defined
        //
        if (isIntegerType(type) &&
            !isNaN(value) &&
            parseInt(value, 10) === value) {
            return true;
        }

        if (isFloatType(type) &&
            typeof value === "number") {
            return true;
        }

        if (type === BasicFieldTypes.layerId &&
            typeof value === "string") {
            return true;
        }

        if (type === BasicFieldTypes.restId &&
            typeof value === "string") {
            return true;
        }

        if (type === BasicFieldTypes.styleId &&
            typeof value === "string") {
            return true;
        }

        if (type === BasicFieldTypes.coordinate &&
            Array.isArray(value) &&
            value.length === 2 &&
            value.every(x => typeof x === "number")) {
            return true;
        }

        if (type === BasicFieldTypes.extent &&
            Array.isArray(value) &&
            value.length === 4 &&
            value.every(x => typeof x === "number")) {
            return true;
        }

        if (Array.isArray(type) &&
            type[0] === BasicFieldTypes.enum &&
            type[1].includes(value)) {
            return true;
        }

        //
        // references objects
        //
        if (Array.isArray(type) && type[0] === Object) {
            // get targeted node and list of required keys in that node
            const targetNode = graph.nodes[type[1]];
            let required = targetNode ? Object.values(targetNode.outgoing).map(x => x.required ? x.name : false).filter(x => x) : null;
            // if no target node was found, log warning
            if (!targetNode) {
                graph.warnings.push(`Node '${node.name}' references '${type[1]}', which does not exist.`);
            }
            else if (Object.keys(value).every(k => {
                // check required field for all contents found
                required = required.filter(x => k !== x);
                // key does not exist in object
                if (!targetNode.outgoing[k]) {
                    return false;
                }
                // check if value matches the value noted in the target node
                return doesValueMatchType(graph, targetNode, {
                    // construct temporary fake edge to see if value matches target edge's requirements
                    ...targetNode.outgoing[k],
                    [key]: value[k]
                }, {key});
            })) {
                // if required keys left, log warnings - given object is incomplete
                if (required.length === 0) {
                    return true;
                }
                graph.warnings.push(`${key} value for '${type[1]}' in '${node.chapterKey}' is missing required field(s) '${required}'.`);
            }
        }

        //
        // arrays
        //
        if (Array.isArray(type) &&
            type[0] === Array &&
            Array.isArray(value) &&
            value.every(v => doesValueMatchType(graph, node, edge, {val: v}, [type[1]]))) {
            return true;
        }
    }

    return false;
}
