/**
 * True in "test", "development", and made-up environments. Vue.js uses
 * the same mechanism to allow webpack to bundle more efficient (but less debuggable)
 * code into the final product. Use "if (devModeFlag)" for development-only code. (HMR, debug, ...)
 * @type {boolean}
 */
const devModeFlag = process.env.NODE_ENV !== "production"; // eslint-disable-line no-undef
// process is indeed undefined in browsers; however, webpack resolves this during transpilation

export default devModeFlag;
