/* Custom types are hardcoded in the Admintool. They exist by contract and are
 * comparable to "Number", "String", and so on for our purposes. */
const customTypes = [
        "Datatypes.Coordinate",
        "Datatypes.Extent"
    ],
    /* Container chapters are not supposed to be within the chapter order and
     * serve as containers for other definitions to inherit from or link to. */
    containerChapters = ["Datatypes"];

export {customTypes, containerChapters};
